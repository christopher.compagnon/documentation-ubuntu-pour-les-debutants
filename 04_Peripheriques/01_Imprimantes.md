# Gestion des imprimantes et impression


## Ajouter une imprimante

**Ubuntu** gère aisément les imprimantes qui sont généralement bien reconnues.

**Remarque** : **Linux** est un système d'exploitation libre. Il doit souvent utiliser du matériel ou du logiciel qui ne l'est pas. Pour que ça puisse fonctionner il faut, la plupart du temps, effectuer de la [rétro-ingénierie](https://fr.wikipedia.org/wiki/R%C3%A9tro-ing%C3%A9nierie), c'est-à-dire comprendre la mécanique interne d'une «boîte noire» à partir de son fonctionnement extérieur. Cette méthode prend du temps et, par conséquent, rend parfois le matériel récent peu ou pas compatible – même si c'est de moins en moins le cas, les constructeurs fournissant de plus en plus des pilotes adéquat ou le code source directement. Il convient cependant de prendre le temps de érifier la compatibilité de son imprimante avec **Linux** avant de l'acheter… on ne sait jamais.

Pour ajouter une imprimantes, il y a généralement deux méthodes :

- par USB;
- par réseau.

### Imprimante USB

Il suffit de brancher l'USB à l'ordinateur pour que le nouveau périphérique – ici l'imprimante – soit reconnu.

Pour vérifier la reconnaissance, se rendre dans **Paramètres > Imprimantes**. La nouvelle imprimante doit apparaître dans la liste.

**Remarque** : L'USB tend à disparaître de plus en plus au profit du réseau, notamment le Wi-Fi. Même si le port USB est toujours disponible, le fabriquant ne fournit pas forcément le câble qui va avec, signe de son désintérêt. Il arrive aussi fréquemment que le fonctionnement USB soit moins performant que le fonctionnement en réseau.

### Imprimante réseau (Wi-Fi)

La connexion Wi-Fi s'effectue de différentes façons :

- **[WPS](https://fr.wikipedia.org/wiki/Wi-Fi_Protected_Setup)** : pour *Wi-Fi Protected Setup* ou, en français, configuration protégée du Wi-Fi. Il s'agit d'une méthode automatisée, semblable à celle pour apparier un périphérique *Bluetooth*, afin de déclarer un nouveau périphérique au réseau, si le routeur possède l'option *WPS* – généralement un bouton. L'appariement s'effectue généralement un appuyant sur un bouton du côté du périphérique – ici l'imprimante – et du routeur. La synthronisation s'effectue alors automatiquement en quelques secondes. Pour toute information complémentaire sur cette méthode, veuillez vous reporter à la documentation de votre imprimante et de votre routeur.
- **WiFi direct** : la méthode consiste à installer un logiciel sur un périphérique de contrôle – ordinateur, téléphone portable, tablette – et de se connecter en direct à l'imprimante, puis de lancer le logiciel qui s'occupera alors de configurer la connexion au réseau.
- **Manuelle** : la méthode est réservée à des utilisateurs avancés. Elle consiste à se connecter directement (Wi-Fi direct) à l'imprimante, puis d'accéder à l'interface Web de gestion du serveur d'impression. Dans la section du paramétrage Wi-Fi, il suffit alors de rentrer les informations correspondant au réseau Wi-Fi comme le SSID et le mot de passe. L'interface dépend du fabriquant et du modèle de l'imprimante.

D'autres méthodes sont disponibles et dépendent de l'imprimante. Pour toute information complémentaire, veuillez vous reporter à la documentation de votre matériel.

Une fois l'imprimante connectée au réseau avec succès, **Ubuntu** trouve le nouveau périphérique dans les secondes qui suivent.
Pour vérifier la reconnaissance, se rendre dans **Paramètres > Imprimantes**. La nouvelle imprimante doit apparaître dans la liste.

![Paramètres -Liste des imprimantes](Ubuntu_parametres_imprimantes.png "Paramètres - Liste des imprimantes")


### Ajout manuel

**Remarque** : Si vous n'êtes pas à l'aise avec l'informatique ou **Linux** et que votre imprimante n'a pas été ajoutée automatiquement en suivant les méthodes décrites plus haut, il est vivement conseillé de faire appel à une personne plus expérimentée qui saura vous aider ou vous guider.

Les imprimantes peuvent être facilement ajoutées manuellement depuis **Paramètres > Imprimantes > Ajouter une imprimante**.

![Paramètres - Ajouter une imprimante](Parametres_ajout_imprimantes.png "Paramètres - Ajouter une imprimante")

Cliquer sur **Ajouter une imprimante**. L'ordinateur recherche les imprimantes disponibles et les propose à l'ajout :

![Paramètres - Ajouter une imprimante manuellement](Parametres_ajout_imprimantes2.png "Paramètres - Ajouter une imprimante manuellement")

**Remarque** : certaines de ces imprimantes sont des imprimantes virtuelles, c'est-à-dire des logiciels qui se comportent comme des imprimantes. Elles permettent, par exemple, d'imprimer un document – image, texte, … – dans un fichier de type [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format) (*Portable Document Format*) ou PS (*PostScript*).

Si l'imprimante n'est toujours pas dans la liste, il est possible de l'ajouter manuellement à partir de son adresse IP, en fournissant cette information dans la zone **Saisissez l'adresse d'un réseau ou recherchez un…**.


## Impression

### Imprimer un document PDF

Vous avez un doument [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format) (reconnaissable à son extension *.pdf*) que vous souhaitez imprimer.

Pour cela : 

1. Allumez votre imprimante; vérifiez qu'elle dispose de papier.
2. Ouvrez votre document (en double-cliquant dessus) avec le *Visionneur de documents* (application par défaut sur *Ubuntu*) ou bien le logiciel que vous utilisez habituellement pour lire les PDFs.
3. Cliquez sur le bouton «*Hamburger*» en haut à droite pour accéder aux options :

![Imprimer - Accéder aux options](Imp_doc_pdf-hamburger.png "Imprimer - Accéder aux options")

4. Cliquez sur la première icône à droite (Imprimante) pour accéder à la liste des imprimantes.
5. Sélectionnez votre imprimante :

![Imprimer - Sélectionner l'imprimante](Imp_doc_pdf-select_printer.png "Sélectionner l'imprimante")

6. Cliquez sur le bouton bleu «*Imprimer*» en haut à droite.

Lors de la sélection de l'imprimante, vous disposez de différentes options comme les pages à imprimer (si vous ne voulez pas tout), le sens d'impression, la mise en page (onglet «*mise en page*») pour définir l'orientation (portrait/paysage), l'échelle, etc.

![Imprimer - Mise en page](Imp_doc_pdf-printer-options.png "Mise en page")

Et d'autres options qui peuvent dépendre de votre imprimante. À vous d'explorer les possibilités.