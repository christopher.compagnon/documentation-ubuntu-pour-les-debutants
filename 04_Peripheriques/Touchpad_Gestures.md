# Les gestes au pavé tactile (*touchpad*)

Un pavé tactile (en anglais : *touchpad*) est un périphérique de pointage au même titre qu'une souris.

Alors que la souris glisse sur le bureau, le pavé tactile reste fixe. Le(s) doigt(s) effectue(nt) les opérations par contact avec la surface tactile du pavé.

## Les différents pavés tactiles

Il existe plusieurs types de pavés tactiles :

- *avec boutons* : en plus de la zone tactile, le pavé possède deux boutons (en haut ou en bas) afin de remplacer le clic gauche/droit de la souris.
- *avec boutons intégrés* : en plus de la zone tactile, le pavé possède deux zones mécaniques (en bas) qui permettent de simuler un clic gauche/droit en appuyant dessus. Le pavé s'enfonce alors sous la pression.
- *sans boutons* : le pavé est un seul bloc tactile, le clic gauche/droit est simulé par un geste, un ou plusieurs doigts positionnés sur le pavé, soit en maintenant une pression, soit en tapant.

En plus de cela, un pavé tactile peut gérer un seul point de contact ou plusieurs, ce dernier étant alors dénommé *multi-point*.

**Remarque** : les gestes dépendent du type de pavé tactile, du nombre de points simultanés gérés par le périphérique ainsi de la configuration. cette dernière est accessible dans **Paramètres > Souris et pavé tactile**.

## Les actions et les gestes associés

En fonction du type et du nombre de points de contact, il est possible d'effectuer un certain nombre d'opérations.

### Positionner/déplacer le pointeur

Quel que soit le type de pavé, pour déplacer le curseur, il suffit de poser le doigt sur la surface tactile et de déplacer son doigt.

![Touchpad : déplacer le curseur](https://upload.wikimedia.org/wikipedia/commons/4/43/Touchpad_gestures_cursor.svg "Touchpad : déplacer le curseur]")

Le curseur suit les mouvements.

### Clic gauche

Pour effectuer un clic gauche, il suffit de *taper sur la zone tactile une fois avec un doigt* :

![Touchpad : clic gauche](https://upload.wikimedia.org/wikipedia/commons/3/3f/Touchpad_gestures_tap1point.svg "Touchpad : clic gauche]")

Pour le double-clic, il suffit de taper *deux fois successivement*.

Si le pavé tactile possède des boutons, alors le bouton gauche fait office de clic gauche.

### Clic droit

Pour afficher le menu contextuel d'un élément, l'équivalent du clic droit avec une souris, il suffit de positionner le curseur sur l'élément en question depuis la zone tactile puis de taper une fois avec deux doigts simultanément :

![Touchpad : clic droit](https://upload.wikimedia.org/wikipedia/commons/2/2c/Touchpad_gestures_tap2points.svg "Touchpad : clic droit")

Le menu contextuel s'affiche. Il ne reste plus qu'à positionner le curseur sur l'option du menu et de valider.

Si le pavé tactile possède des boutons, alors le bouton droit fait office de clic droit.

### Sélectionner, glisser et déposer

Cette opération s'effectue avec 2 doigts. Un premier en bas à gauche qui reste posé durant toute l'opération; le second, au milieu, qui sert à déplacer l'objet depuis sa position d'origine jusqu'à sa destination :

![Touchpad : sélectionner, glisser et déposer](https://upload.wikimedia.org/wikipedia/commons/6/60/Touchpad_gestures_selectdrag.svg "Touchpad : sélectionner, glisser et déposer")

Ce geste est aussi utilisé pour redimensionner une fenêtre.

### Défilement vertical haut/bas (ascenseur)

Pour monter ou descendre sur une page ou le contenu d'une fenêtre, poser un doigt sur le bord externe – à droite ou à gauche – et glisser vers le haut ou le bas :

![Touchpad : ascenseur](https://upload.wikimedia.org/wikipedia/commons/0/00/Touchpad_gestures_scroll_up%2Bdown_right.svg "Touchpad : ascenseur 1 doigt")

**Remarque** : cette opération n'est possible que si l'option **Défilement aux bords** est activée dans **Paramètres > Souris et pavé tactile**.

L'opération peut aussi s'effectuer avec deux doigts :

![Touchpad : ascenseur](https://upload.wikimedia.org/wikipedia/commons/0/0a/Touchpad_gestures_scroll2points.svg "Touchpad : ascenseur 2 doigts")

**Remarque** : cette opération n'est possible que si l'option **Défilement à deux doigts** est activée dans **Paramètres > Souris et pavé tactile**.

### Défilement horizontal gauche/droite

Pour naviguer horizontalement une page ou une fenêtre (ou changer d'écran virtuel), il suffit de faire glisser deux doigts horizontalement sur la zone tactile :

![Touchpad : Défilement horizontal gauche/droite](https://upload.wikimedia.org/wikipedia/commons/c/ca/Touchpad_gestures_2hpoints.svg "Touchpad : Défilement horizontal gauche/droite")

**Remarque** : cette opération n'est possible que si l'option **Défilement à deux doigts** est activée dans **Paramètres > Souris et pavé tactile**.

### Zoom

Pour zoomer, il suffit de poser deux doigts – généralement le pouce et l'index – sur la diagonale de la zone tactile et de les écarter pour augmenter le zoom :

![Touchpad : zoom in](https://upload.wikimedia.org/wikipedia/commons/a/af/Touchpad_gestures_zoomin.svg "Touchpad : zoom in")

ou de les rapprocher, c'est-à-dire «pincer», pour diminuer le zoom :

![Touchpad : zoom out](https://upload.wikimedia.org/wikipedia/commons/0/0b/Touchpad_gestures_zoomout.svg "Touchpad : zoom out")

### Activités

Pour afficher les *Activités*, il suffit de glisser simultanément 3 doigts sur la zone tactile vers le haut – pour afficher les activités :

![Touchpad : Activités](https://upload.wikimedia.org/wikipedia/commons/5/52/Touchpad_gestures_activities.svg "Touchpad : Activités")

ou vers le bas  – sens inverse – pour revenir à l'affichage normal.

### Changement d'écran virtuel

Pour passer d'un écran virtuel à un autre, il suffit de glisser simultanément 3 doigts sur la zone tactile vers la gauche – pour passer à l'écran suivant – ou vers la droite – pour passer à l'écran précédent :

![Touchpad : Changement d'écran virtuel 3 points](https://upload.wikimedia.org/wikipedia/commons/8/83/Touchpad_gestures_3hpoints.svg "Touchpad : Changement d'écran virtuel 3 points")

L'opération peut également s'effectuer avec deux doigts :

![Touchpad : Changement d'écran virtuel 2 points](https://upload.wikimedia.org/wikipedia/commons/c/ca/Touchpad_gestures_2hpoints.svg "Touchpad : Changement d'écran virtuel 2 points")

**Remarque** : cette opération n'est possible que si l'option **Défilement à deux doigts** est activée dans **Paramètres > Souris et pavé tactile**.

