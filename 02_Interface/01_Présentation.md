# Présentation de l'interface


## Écran de connexion (ou de session)

Au lancement, l'écran de l'ordinateur ressemble à ceci :

![Écran de connexion](Ubuntu-Login-screen.png "Écran de connexion")

Au milieu de l'écran, le ou les utilisateurs enregistrés sur la machine.
La zone de mot de passe qui s'active quand on choisit un utilisateur.

**À noter** : lorsque le curseur de la souris est dans la zone du mot de pass, une roue crantée apparaît en bas à droite. Elle permet de changer de moteur d'affichage.
Par défaut, le moteur est **Wayland**. L'ancienne version était **Xorg**. Cette dernière est toujours disponible en cas de problème ou d'incompatibilité, comme par exemple le cas de *TeamViewer*.


Lorsque l'utilisateur tape le mot de passe associé à son utilisateur, il entre dans sa **session**.

![Session Ubuntu par défaut](Ubuntu-session-opened.png "Session Ubuntu par défaut")

**À noter** : cette disposition peut différer en fonction de la configuration appliquée par la suite comme le fond d'écran, le thème ou le positionnenent de la barre de tâche, nommée aussi *Dock*.
En effet, la position de ce dernier peut être modifiée depuis **[Paramètres > Apparence](02_Paramètres.md)**. Par exemple, avec un positionnement en bas :

![Bureau Ubuntu dock en bas](Ubuntu_bureau.png "Ubuntu dock en bas")

## Description de l'interface

Lors de la connexion, avant toute opération, l'écran est divisé en 3 zones :
- le bureau (au centre);
- la barre de menus (en haut);

![Bureau barre de menus](Ubuntu_Bureau_barre_menus.png "Ubuntu barre de menus")

- la barre de tâches ou *Dock* (à gauche par défaut).

![Bureau barre de tâches](Ubuntu_Bureau_barre_taches.png "Ubuntu barre de tâches")

### Le bureau

Cette zone contrale ne sert pas à grand-chose à part afficher un fond d'écran (nommé aussi *arrière-plan*) pour des raisons esthétiques et quelques icônes (si cette option a été activée).

Il est possible de personnaliser le fond d'écran depuis les **[Paramètres > Arrière-plan](./02_Paramètres.md)** ou bien en cliquant dessus avec le boutons droit de la souris.

L'affichage des icônes est personnalisable depuis  **[Paramètres > Apparence](./02_Paramètres.md)**.

#### Les bureaux virtuels

L'informatique étant virtuelle, le bureau ne fait pas exception à la règle puisqu'il est virtuel, c'est-à-dire qu'il n'existe par réellement. Comme il n'existe pas réellement, il est possible d'en créer plusieurs, autant qu'on veut (tant qu'il y a de la ménoire disponible). Tout comme les bureaux physiques, chaque bureau aura son *propre encombrement*, c'est-à-dire des applications qui s'y afficheront.

Un bureau virtuel est un donc bureau identique à celui que vous avez sous les yeux, mais il n'est pas affiché.

Tout comme le bureau par défaut qui est affiché à l'écran, il s'afficherait sur un second écran si vous en aviez un. Mais comme cet écran n'existe pas, il existe *virtuellement*, quelque part dans la mémoire de l'ordinateur, attendant d'être sélectionné pour être affiché à l'utilisateur.
Lorsque vous basculez sur un écran virtuel, le bureau actuel devient alors virtuel, attendant son tour.

Linux gèrent des bureaux virtuels.

Ces bureaux virtuels permettent d'éviter l'encombrement du bureau principal en permettant une organisation personnalisée en séparant, par exemple, les activités.

Ainsi, nous parlons de **bureaux virtuels**. L'écran n'affiche qu'un seul bureau virtuel à la fois. Il est possible de passer de l'un à l'autre. Il est possible de passer une application de l'un à l'autre par simple glisser/déposer.

Par l'intermédiaire de son interface, **Ubuntu** gère des bureaux virtuels dynamiquement. Par défaut au nombre de deux, le sytème d'exploitation en crée immédiatement un de plus à chaque fois que vous en utilisez un nouveau.

Pour afficher les bureaux virtuels, il suffit de cliquer sur le bouton *Applications* ou *Activités*.
Les bureaux virtuels apparaissent sous la forme de miniatures, sous la zone de recherche, en haut de l'écran. Il suffit de cliquer dessus pour le visualiser en grand.

### La barre de menus

Cette barre est située par défaut en haut de l'écran.

![Bureau barre de menus](Ubuntu_Bureau_barre_menus.png "Ubuntu barre de menus")

Elle affiche :

- à gauche : un menu *Activités* qui permet d'afficher les activités;

![Bureau activités](Ubuntu_Bureau_Zone_activites.png "Bureau activités")

- au centre : **la zone de notifications**, réduite par défaut à l'affichage de la date du jour et l'heure;

![Bureau noticications](Ubuntu_Bureau_Zone_notifications.png "Bureau noticications")

- à droite : **la zone de statut**, réduite par défaut à l'affichage d'icônes.

![Interface - zone de statut](Ubuntu_Bureau_Zone_statut.png "Interface - zone de statut")


#### Activités

En cliquant sur *Activités* , l'écran change de disposition et affiche un aperçu des activités en cours, c'est-à-dire des applications ouvertes (au centre) ainsi que les bureaux virtuels disponibles (en haut), une zone de recherche en haut et la barre de tâches (*Dock*) pour accéder à vos applications favorites.

![Activités](Ubuntu-activities.png "Activités")

*Activités* sert à afficher en un seul coup d'œil vos activités et passer d'une application à une autre simplement en cliquant sur la fenêtre correspondante.

En tapant un mot dans la zone de recherche, vous lancez une recherche sur les applications et les fichiers.
Si vous désirez lancer une application dont vous ne vous rappelez plus l'emplacement, il suffit de la rechercher ici.

En cliquant sur un bureau virtuel ou un autre (affichage sous la zone de recherche), vous passez sur le bureau virtuel correspondant.

**Astuce** : Les *Activités* sont accessibles par la touche «Windows» reconnaissable par son logo *Windows*, nommée aussi «Super» dans le monde Linux (car tous les claviers n'ont pas forcément une touche avec un logo *Windows* dessus). Un second appui sur la touche bascule à l'affichage d'origine (annulation).

#### Zone de notifications

En cliquant sur la partie centrale de la barre de menus, on déploie la zone de notifications :

![Zone de notifications par défaut](shell-appts.png "Zone de notifications par défaut")

Cette zone affiche le calendrier, les notifications système, le contrôles des media en cours de lecture le cas échéant,…

#### Menu système

En cliquant sur la gauche de la barre de menus, on déploie les menus :

![Menu système](shell-exit.png "Menu système")

Parmi ces menus, il y a :
- le contrôle du son et de la luminosité de l'écran;
- les connexions réseaux filaire et Wi-Fi;
- les connexions Bluetooth;
- l'accès aux paramètres;
- les opérations sur la session : verrouiller, quitter la session, éteindre la machine, …

Cette zone affiche par défaut la connexion réseau, le réglage du son et l'accès à des options de démarrage.

En cliquant dessus, un menu se déroule :

![Interface - menu zone de statut](Ubuntu_ZoneStatut_Menu.png "Interface - menu zone de statut")

Il donne accès au réglage du son, à la connexion réseau, 

![Interface - menu réseau](Ubuntu_ZoneStatut_Reseau.png "Interface - menu réseau")

à la gestion de l'énergie,

![Interface - menu énergie](Ubuntu_ZoneStatut_Energie.png "Interface - menu énergie")

aux **[Paramètres](./02_Paramètres.md)**, au verrouillage de la session et à l'extinction de la machine.

![Interface - menu arrêt](Ubuntu_ZoneStatut_Menu_demarrer.png "Interface - menu arrêt")

### La barre de tâches (Dock)

![La barre de tâches](shell-activities-dash.png "La barre de tâches")

La barre des tâches affiche par défaut :
- le bouton d'accès aux applications (dans l'exemple ici : le bouton à droite);
- les applications favorites (dans l'exemple ici : les icônes à gauche du bouton d'applications);
- les applications lancées (dans l'exemple ici : les icônes qui sous marquées d'un point au dessous).

**À noter** : Les applications favorites sont séparées des autres par un séparateur vertical. À gauche de ce séparateur, les applications favorites. À droite de ce séparateur, les applicatons lancées non pésentes dans les favoris.

Les applications lancées, favorites ou pas, sont toutes identifiées pas un ou plusieurs points sous leur icône. Chaque point repérsente une instance de l'application, c'est-à-dire le nombre de fois que l'application est lancée (si celle-ci le permet, certaines applications ne pouvant lancer qu'une seule instance).

#### Afficher les applications

Par défaut, lorsque la barre de tâches est située à gauche, le bouton est en bas à gauche de l'écran.
Lorsque la barre de tâches est située en bas, le bouton est en bas à droite de l'écran.

![Afficher les applications](Ubuntu_Bureau_Bouton_applications.png "Afficher les applications")

Depuis cette icône, vous accédez aux [applications installées](./02_Liste_applications.md) sur votre ordinateur.

#### Navigation depuis le clavier

Pour passer d'un écran virtuel à un autre, il existe une combinaison de touches :
- [ctrl] + [alt] + (flèche droite) : basculer sur l'écran virtuel suivant (s'il existe);
- [ctrl] + [alt] + (flèche gauche) : basculer sur l'écran virtuel précédent (s'il existe).

**À noter** : ces combinaisons de touches peuvent être changées dans les **[Paramètres](./02_Paramètres.md) > Clavier > Raccourcis clavier > Voir et personnaliser les raccourcis** .

![Paramètres - Clavier](Ubuntu_Parametres_Clavier.png "Paramètres - Clavier")

#### Navigation depuis la souris

Pour passer d'un écran virtuel à un autre à l'aide de la souris, il existe deux méthodes :
- en cliquant sur le bouton *Applications* ou *Activités*, les bureaux virtuels apparaissent alors sous la forme de miniatures, sous la zone de recherche, en haut de l'écran.
- en positionnant le pointeur de la souris et en faisant rouler la molette (il faut une souris à molette) vers le haut pour basculer sur l'écran virtuel suivant (s'il existe), vers le bas pour basculer sur l'écran virtuel précédent (s'il existe).

#### Navigation depuis le touchpad

En utilisant 3 doigts simultanément, pour glisser à droit ou à gauche pour accéder au bureau virtuel correspondant.

![Bureaux virtuels - 3 doigts](three-finger-swipe-gnome.webp "Bureaux virtuels - 3 doigts")
