# Personnalisation de l'interface

Si certains regrettent l'absence de paramétrages avancés pour modifier l'affichage, l'interface (nommée **GNOME**) est pensée pour être simple. En comparaison d'autres environnements de bureau, **GNOME** propose peu de modifications. Il est toutefois possible d'étendre ses fonctionnalités par des extensions.

Certains éléments d'affichage peuvent toutefois être changés aisément.

## Changer l'arrière-plan (fond d'écran)

Grâce à un clic-droit sur le fond d'écran du bureau, choisir l'option *Changer l'arrière-plan*.
Cette action ouvre le panneau de contrôle **[Paramètres > Arrière-plan](../Paramètres/Paramètres.md#arrière-plan)**.

## Positionnement de la barre de tâches (Dock)

Si elle est par défaut située à gauche sous Ubuntu, la barre de tâches peut être déplacée et positionnée à gauche, en bas ou à droite depuis **[Paramètres > Apparence](../Paramètres/Paramètres.md#apparence)**.


## Depuis la logithèque

Certains thèmes sont disponibles depuis la logithèque **Logiciels > Personnalisation**.

## Changer la position et le comportement du Dock

Par défaut à gauche sur Ubuntu, il est possible de le déplacer et de changer son comportement.
Par exemple, grââce à l'extension *Dash To Panel*, le *Dock* devient un *Panel* à la Windows, ,rassemblant dans une barre de tâches en bas, sur toute la largeur de l'écran, le *Dock* et le menu des *Activités*, avec les applications favorites à gauche et les raccourcis vers les paramètres et les notifications à droite.

![Dash To Panel](Dash-To-Panel.png "Dash To Panel")

Il existe bien d'autres extensions pour modifier les affichages, les dispositions et ajouter des indicateurs de statut.

## Ajout d'extensions

**GNOME** est pensé pour être à la fois simple et efficace, avec peu d'options pour éviter de perdre les utilisateurs, notamment les débutants, dans un dédale de paramètres.

Les extensions permettent de modifier le comportement et l'apparence du système. Ils sont disponibles soit depuis [le site web](https://extensions.gnome.org/).

### Depuis le site web

Le site web fonctionne de manière intéractive avec le système. Pour cela, il faut installer un composant nécessaire à son fonctionnement.

Ouvrez un terminal (depuis Applications > Terminal) et tapez :

    sudo apt-get install chrome-gnome-shell

Validez par [Entrée] et l'interface demande un mot de passe, celui de l'administrateur, il s'agit du mot de passe que vous utilisez pour vous connecter à votre session.

**Note** : pour savoir si vous avez les droits, voir la section **Astuces > Savoir si on a les droits de l'administrateur**.

Une fois que ce outil est installé, vous pouvez ajouter les extensions depuis [le site web](https://extensions.gnome.org/). Il vous faudra peut-être rafraîchir la page web pour actualiser la détection.