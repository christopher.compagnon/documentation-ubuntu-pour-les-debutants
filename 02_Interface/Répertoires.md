# Organisation des répertoires

Lorsque vous lancez votre gestionnaire de fichiers, vous trouverez l'arborescence des répertoires organisée de la façon suivante :

- **Dossier personnel** : dans l'univers Linux/Unix, il s'agit du répertoire *HOME*, qui se trouve précisément à /home/{utilisateur}. Chaque utilisateur possède un répertoire *HOME* à lui dans lequel se trouve l'ensemble de ses fichiers et de ses configurations. Il suffit de sauvegarder et restaurer ce répertoire sur une autre machine ou une installation fraîche pour retrouver son environnement à l'image de l'original.

Dans ce répertoire se trouvent les répertoires suivants :

- **Documents** :
- **Images** :
- **Musique** :
- **Téléchargements** :
- **Vidéos** :
- **Corbeille** :
