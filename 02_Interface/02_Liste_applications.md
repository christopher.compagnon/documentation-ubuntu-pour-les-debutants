# Les applications Ubuntu

La distribution **Ubuntu** est fournie avec un ensemble de logiciels par défaut.

Nous l'avons vu dans [la présentation des distributions Linux](../À_propos.md), Ubuntu propose un ensemble de logiciels pré-installés afin de permettre l'utilisation immédiate du système d'exploitation.

Si ces logiciels ont généralement un nom propre, ils sont également disponibles sous une terminologie plus facile à comprendre et retenir pour le commun de mortels.

Les logiciels sont accessibles depuis le boutons **Afficher les applications** de la barre d'icônes (Dash).

## Pilotes additionnels

![Application - pilotes additionnels](system-component-driver.png "Application - pilotes additionnels")

Outil d'installation de pilote de périphériques.

Capable d'analyser tous les périphériques matériels et de télécharger automatiquement les pilotes appropriés (certains sont des logiciels propriétaires).

## Logithèque

![Application - logithèque](software-store.png "Application - logithèque")

Pour rechercher, trouver, ajouter et supprimer des applications, ainsi que mettre à jour l'ensemble du système Ubuntu avec les applications installées.

Les applications Ubuntu sont fournies aux utilisateurs dans deux choix de format, le [DEB](https://fr.wikipedia.org/wiki/Deb]) (paquet standard) et le tout nouveau **Snap**. Le serveur central sur Internet qui sert les logiciels Ubuntu est appelé *référentiel*. La plupart des applications servies dans le logiciel Ubuntu sont des logiciels open source gratuits et certaines sont propriétaires.

## Configuration réseau avancée

![Application - Configuration réseau avancée](network-workgroup.png "Application - Configuration réseau avancée")

Interface graphique qui offre plus de configurations que celle que vous trouvez dans les paramètres système tels que le point d'accès (hotspot), VPN, PPTP. etc.

## Gestionnaire d'archives

![Application - Gestionnaire d'archives](file-roller.png "Application - Gestionnaire d'archives")

le **Gestionnaire d'archives** est l'utilitaire de compression. Utilisez-le pour lire et écrire des fichiers d'archives *ZIP*, *TAR*, *7Z*, *XZ* et RAR avec ou sans compression et protection par mot de passe.

Il prend en charge les meilleurs formats de compression *7Z* et *XZ*, ainsi que l'archive standard du monde Unix *TAR* et le populaire *RAR*. Par exemple, vous pouvez créer un fichier d'archive nommé **monfichiersecret.zip** avec un mot de passe afin que toute personne recevant une copie ne puisse pas l'ouvrir ni voir son contenu à moins de connaître le mot de passe.

## Sauvegardes

![Application - Sauvegardes](backups-app.png "Application - Sauvegardes")

Outil de sauvegarde.

Également appelé **DejaDup**, il peut effectuer des tâches de sauvegarde : planifier, stocker et restaurer des sauvegardes pour l'ensemble de l'ordinateur ainsi que pour certaines parties de celui-ci, selon le souhait de l'utilisateur.

## Calculatrice

![Application - Calculatrice](calculator-app.png "Application - Calculatrice")

À des fins de calcul avec les modes scientifiques et financiers. Par exemple, il peut calculer 1 + 1 ainsi que convertir les km en cm et le dollar américain en une autre devise, entre autres fonctionnalités.

Cette application fait partie de **GNOME**.

## Agenda

![Application - Agenda](calendar-app.png "Application - Agenda")

Calendrier et planificateur.

Utilisez-le pour afficher la date, créer un calendrier et définir un rappel. Le calendrier prend en charge les couleurs et l'accès aux services en ligne externes tels que ceux de Google et d'autres. 

Cette application fait partie de **GNOME**.

## Webcam

![Application - Webcam](gnome-snapshots.png "Application - Webcam")

Application webcam. 

Utilisez-le pour travailler avec des appels vidéo, des enregistrements d'écran, etc. Vous pouvez enregistrer des vidéos et prendre des photos d'une manière très simple. 

Cette application fait partie de **GNOME**.

## Caractères

![Application - Caractères](accessories-character-map.png "Application - Caractères")

Outil personnages et émoticônes. 

Affiche tous les caractères du jeu de caractères disponible sur l'ordinateur. 

Cette application fait partie de **GNOME**.

## Disques

![Application - Disques](disk-utility-app.png "Application - Disques")

Utilitaire de disque. Affiche, crée, redimensionne, formate, sauvegarde/restaure et supprime des disques et des partitions avec une capacité d'analyse comparative et de test SMART (santé du disque dur). Il prend en charge plusieurs systèmes de fichiers, à savoir FAT, exFAT et NTFS (Windows) et EXT4, Btrfs et XFS (GNU/Linux), avec prise en charge de la technologie de cryptage de disque. Il vous permet également de créer un fichier image ISO à partir de CD ou de lecteurs de disque ainsi que d'écrire des fichiers ISO pour créer un disque flash USB amorçable. Cette dernière fonctionnalité chevauche le créateur de disque de démarrage.

## Analyseur d'utilisation des disques

![Application - Analyseur d'utilisation des disques](disk-usage-app.png "Application - Analyseur d'utilisation des disques")

Analyseur d'utilisation du disque (également connu sous le nom de **Baobab**) Analyse et affiche le classement des dossiers en fonction de leur taille avec un graphique. L'utilisateur peut choisir n'importe quel disque ou dossier spécifique, puis analyser rapidement son contenu. C'est très utile par ex. pour les utilisateurs ayant une capacité de disque limitée.

## Visionneur de documents

![Application - Visionneur de documents](evince.png "Application - Visionneur de documents")

Lecteur PDF. Capable de lire des livres électroniques, des bandes dessinées et le format de millions de livres sur Internet, DJVU. En plus de lire vos documents de travail quotidiens, Document Viewer, par exemple, peut également lire les livres numériques d'Internet Archive.

## Numériseur de documents

![Application - Numériseur de documents](scanner.png "Application - Numériseur de documents")

Outil de numérisation. Utilisez-le avec votre scanner pour créer un document numérique à partir de votre document physique. Il peut enregistrer le document au format PDF ou Bitmaps. Par exemple, vous pouvez numériser vos anciennes photos et les résultats peuvent être des photos numériques stockées au format JPEG.

## Fichiers

![Application - Fichiers](filemanager-app.png "Application - Fichiers")

Gestionnaire de fichiers.

**Fichiers** (également connus sous le nom de **Nautilus**) permettent à l'utilisateur d'accéder aux fichiers et dossiers ainsi qu'à tous les lecteurs de disque, y compris les lecteurs USB externes. C'est le composant central que tous les utilisateurs d'Ubuntu utilisent certainement. 

Cette application fait partie de **GNOME**.

## Firefox

![Application - Firefox](firefox.png "Application - Firefox")

Navigateur Internet.

Pour la plupart des utilisations Internet : pour explorer le Web, accéder aux courriers Web, télécharger et regarder des vidéos en streaming. Cette application est créée par Mozilla et est la plus populaire parmi les utilisateurs d'ordinateurs depuis 1998 et est fournie avec Ubuntu depuis la première version 4.04 *Warthy Warthog*.

## Polices

![Application - Polices](preferences-desktop-font.png "Application - Polices")

Utilitaire d'installation des polices.

Affiche les polices de caractères et installe les fichiers de polices. Par exemple, cette application peut présenter plusieurs polices regroupées dans Ubuntu, à savoir *Liberation Serif* et *FreeSerif* (remplacement de *Times New Roman*), *Liberation Sans* et *FreeSans* (remplacement d'*Arial*) et *Liberation Mono* ainsi que *FreeMono* (remplacement de *Courier Mono*). De plus, toutes les polices préinstallées avec Ubuntu sont toutes des logiciels gratuits, c'est-à-dire sans restriction à des fins commerciales et de redistribution.

## Gnome

![Application - Gnome](gnome-logo-text.svg.png "Application - Gnome")

Tout le bureau. 

**GNOME** est l'interface utilisateur d'Ubuntu, ses expériences, son menu et plusieurs applications de base pour l'utilisateur. Le bureau d'Ubuntu comporte un bouton *Activités*, un panneau supérieur de couleur noire et un menu de démarrage sur tout l'écran, tandis que chaque fenêtre d'application avait une barre de titre épaisse. De nombreux composants d'Ubuntu proviennent de **GNOME**, par exemple le calendrier, les disques et les paramètres. Cette technologie est réalisée par le projet **GNOME**.

## Aide

![Application - Aide](help-contents.png "Application - Aide")

Documentation et le guide d'utilisation. Vous pouvez lire comment travailler avec Ubuntu Desktop dans cette application. Cette aide s'ouvrira également lorsque vous cliquerez sur Aide sur chaque application Ubuntu sous son bouton de menu hamburger pour afficher le guide de l'utilisateur de l'application. Par exemple, disons Calculatrice, pour ouvrir le manuel d'utilisation, cliquez sur le menu hamburger > Aide > le livre s'ouvrira.

## Visionneur d'images

![Application - Visionneur d'images](image-viewer-app.png "Application - Visionneur d'images")

Visionneur d'images. 

Visionneur d'images (également connu sous le nom d'Eye of GNOME) affiche rapidement des photos et des images. Peut également afficher des informations cachées sur une image. Il prend en charge tous les formats d'image tels que JPEG, PNG, GIF, TIFF et même SVG.

## LibreOffice

![Application - LibreOffice](libreoffice-main.png "Application - LibreOffice")

Application de bureau **LibreOffice**.

**LibreOffice** peut produire des documents ainsi que des PDF en un seul clic. Il est divisé en cinq composants, à savoir *Writer* le traitement de texte, *Calc* la feuille de calcul, *Impress* la présentation, *Draw* l'outil de création de diagrammes et l'éditeur PDF, et *Math* l'éditeur d'équations. Writer peut lire et écrire des documents ODT et Word, Calc prend en charge les documents ODS et Excel, tandis qu'Impress prend en charge les documents ODP et PowerPoint.

## LibreOffice Writer

![Application - LibreOffice Writer](libreoffice-writer.png "Application - LibreOffice Writer")

**Writer** (icône bleue) est l'alternative à *Microsoft Word*, pour ouvrir et éditer des documents texte dans tous les formats de texte doc, docx, html, pdf, rtf, odt et txt. Il peut créer des pdf et des [epub](https://fr.wikipedia.org/wiki/EPUB_(format)) en un seul clic. Les gens l'utilisent pour rédiger des textes simples, des lettres, des invitations, des livres, des brochures, des pages Web, des devoirs scolaires et même des thèses universitaires. Il s'agit du traitement de texte Ubuntu depuis la première version jusqu'à maintenant. Son format par défaut est *odt*.

## LibreOffice Calc

![Application - LibreOffice Calc](libreoffice-calc.png "Application - LibreOffice Calc")

**Calc** (icône verte) est l'alternative à *Microsoft Excel*, pour ouvrir et modifier le tableur aux formats xls, xlsx et ods. Il est compatible avec presque toutes les fonctions Excel telles que la somme, la moyenne, la recherche virtuelle et des fonctionnalités telles que le publipostage et le tableau croisé dynamique. Il peut créer un pdf en un seul clic. Son format par défaut est *ods*.

## LibreOffice Impress

![Application - LibreOffice Impress](libreoffice-impress.png "Application - LibreOffice Impress")

Impress (icône rouge/orange) est l'alternative à *Microsoft PowerPoint*, pour ouvrir et créer des diapositives aux formats ppt, pptx et odp. Il peut créer des pdf et des [epub](https://fr.wikipedia.org/wiki/EPUB_(format))  en un seul clic. Son format par défaut est *odp*.

## LibreOffice Draw

![Application - LibreOffice Draw](libreoffice-draw.png "Application - LibreOffice Draw")

**Draw** (logo jaune) est l'alternative à *Microsoft Visio*, qui est à la fois illustrateur et éditeur PDF. **Draw** est un éditeur graphique vectoriel doté de capacités d'organigrammes et de diagrammes. Les formats de fichiers qu'il peut gérer sont eps, html, jpeg, pdf, png, svg et tiff.

## LibreOffice Math

![Application - LibreOffice Math](libreoffice-math.png "Application - LibreOffice Math")

**Math** (icône rose) est l'éditeur d'équations, une application autonome de LibreOffice pour écrire des équations mathématiques comme la multiplication, la division, la racine carrée, le sigma, les intégrales et les différentielles, etc. Il est bien intégré dans Writer et il est recommandé de l'utiliser de cette façon. Il est particulièrement utile pour les enseignants et les étudiants qui rédigent des manuels et des thèses en mathématiques, physique, chimie, etc.

## Mots de passe et clés

![Application - Mots de passe et clés](passwords-app.png "Application - Mots de passe et clés")

**Mots de passe et clés** est le gestionnaire d'informations d'identification. Il (également connu sous le nom de **Seahorse**) permet aux utilisateurs avancés de gérer plus facilement les clés GPG3 : ajout, stockage, suppression, etc.

## Statistiques d'alimentation 

![Application - Statistiques d'alimentation](power-statistics.png "Application - Statistiques d'alimentation")

**Statistiques d'alimentation** est la visionneuse de détails sur la batterie. Utilisez-le pour vérifier l'état de santé de la batterie de votre ordinateur portable et les conditions actuelles telles que la charge maximale. capacité et capacité de charge ainsi que le pourcentage de charge. Les statistiques de puissance sont bonnes à utiliser avant d'acheter un ordinateur portable ou à exécuter pour la première fois après en avoir acheté un pour vous assurer que votre batterie est saine.

## Remmina

![Application - Remmina](org.remmina.Remmina.png "Application - Remmina")

**Remmina** est l'application de bureau à distance. Utilisez-le pour contrôler visuellement à distance un autre ordinateur du réseau. Ainsi, l'écran de l'ordinateur distant pourra contrôler et cliquer sur le bureau d'autres personnes sur votre bureau.

## Rhythmbox

![Application - Rhythmbox](rhythmbox.png "Application - Rhythmbox")

**Rhythmbox** est le lecteur audio. Lit de la musique, des enregistrements ainsi que des podcasts en ligne et hors ligne. Prend en charge la recherche, les listes de lecture, la répétition/lecture aléatoire, les favoris et l'importation automatique de chaque fichier audio dans le répertoire Musique dans la maison. Peut lire des fichiers MP3, OGG, OPUS, WAV et autres fichiers audio. Pour jouer avec **Rhythmbox**, double-cliquez simplement sur un fichier MP3 et il sera lu – et il en va de même pour les autres fichiers audio.

## Paramètres

![Application - Paramètres](system-settings.png "Application - Paramètres")

**Paramètres** est le panneau de contrôle. Utilisez-le pour configurer votre ordinateur Ubuntu. Il présente les configurations verticalement sous la forme d'une liste commençant par Réseaux, pour configurer le wifi et autres, et se terminant par À propos, pour afficher les spécifications de votre ordinateur.

## Shotwell

![Application - Shotwell](shotwell.png "Application - Shotwell")

**Shotwell** est l'organisateur de photos. Affiche les photos et les images, comme la **Visonneur d'images**, et les modifie davantage avec quelques options (recadrer, éclaircir, assombrir). Importe des photos de votre appareil photo via un câble USB, puis les gère de manière pratique et ordonnée. Pour ouvrir avec Shotwell, cliquez simplement avec le bouton droit sur un fichier JPEG > Ouvrir avec une autre application > sélectionnez Shotwell > Sélectionner > l'image sera ouverte. Pour recadrer, cliquez sur le bouton Recadrer en bas > ajustez le rectangle à votre guise > Recadrer > Enregistrer.

## Applications de démarrage

![Application - Applications de démarrage](session-properties.png "Application - Applications de démarrage")

**Applications de démarrage** est la configuration de démarrage. Toute application que vous avez répertoriée dans **Applications de démarrage** s'exécutera automatiquement à chaque démarrage de l'ordinateur. En revanche, en supprimant un des applications de démarrage, celui-ci ne s'exécutera plus automatiquement au moment du démarrage.

## Logiciels et mises à jour

![Application - Logiciels et mises à jour](software-properties.png "Application - Logiciels et mises à jour")

**Logiciels et mises à jour** (logo violet ; à ne pas confondre avec **Gestionnaire de mises à jour**) est la configuration du référentiel. C'est l'une des configurations les plus importantes pour chaque utilisateur d'Ubuntu. Utilisez-le pour configurer des référentiels, choisir un miroir de pays, ajouter/supprimer des référentiels, activer la correction de bugs et les mises à jour de sécurité et activer LivePatch.

## Gestionnaire de mises à jour

![Application - Gestionnaire de mises à jour](software-updater.png "Application - Gestionnaire de mises à jour")

**Gestionnaire de mises à jour** (logo gris ; à ne pas confondre avec **Logiciels et mises à jour**) : auto-décrit. Utilisez-le pour vérifier les mises à jour disponibles sur Internet et mettre à jour Ubuntu. Cela résout les problèmes et renforce la sécurité d'Ubuntu. S'il existe une nouvelle version d'Ubuntu, Software Updater vous en informera automatiquement. L'avantage d'Ubuntu est la possibilité de mettre à niveau complètement le système d'exploitation ainsi que toutes les applications installées.

## Créateur de disque de démarrage

![Application - Créateur de disque de démarrage](usb-creator-gtk.png "Application - Créateur de disque de démarrage")

**Créateur de disque de démarrage** : le créateur de démarrage. Cela permet à l'utilisateur d'Ubuntu de graver une image ISO sur une clé USB au lieu d'un CD afin que ce support puisse être utilisé comme support de démarrage. Un support de démarrage est un stockage sur disque permettant d'exécuter, d'installer ou de prévisualiser un système d'exploitation sur un ordinateur. Par exemple, vous pouvez graver un fichier image Ubuntu sur un lecteur flash de 8 Go avec cette application et, par conséquent, l'USB peut être utilisé pour installer Ubuntu sur n'importe quel ordinateur.

## Moniteur système

![Application - Moniteur système](system-monitor-app.png "Application - Moniteur système")

**Moniteur système** : le visualiseur de ressources. Comme un compteur de vitesse, il affiche l'utilisation des ressources système et la liste de tous les programmes actifs. Par exemple, vous pouvez voir la quantité de mémoire utilisée/libre en Mo ainsi que le graphique haut et bas des mesures du pourcentage de CPU.

## Éditeur de texte

![Application - Éditeur de texte](org.gnome.TextEditor.png "Application - Éditeur de texte")

**Éditeur de texte** est votre application pour écrire du texte clair ainsi que du code dans les langages de programmation. Il propose la numérotation des lignes, la vérification orthographique et bien plus encore. Il prend en charge la palette de couleurs pour votre commodité, ainsi que des plugins pour l'étendre. 

Cette application fait partie de **GNOME**.

## Terminal

![Application - Terminal](terminal-app.png "Application - Terminal")

**Terminal** : les commandes. Les utilisateurs d'Ubuntu pratiqueront beaucoup **Terminal**, principalement en copiant et en collant des lignes de commande à partir de livres ou d'Internet, car elles permettent de faire les choses plus rapidement et automatiquement. Consultez le Guide de l'utilisateur complet d'Ubuntu pour apprendre à utiliser **Terminal**.

## Thunderbird

![Application - Thunderbird](Thunderbird_2023_icon.png "Application - Thunderbird")

**Thunderbird** : le client de messagerie. Il s'agit d'une boîte postale numérique où l'utilisateur peut lire et envoyer des e-mails localement. Les utilisateurs professionnels, professionnels et individuels très occupés l'utilisent souvent beaucoup. Utilisez-le pour accéder à Gmail ou aux services de messagerie. 

Visitez le [Guide rapide de Thunderbird](https://support.mozilla.org/fr/products/thunderbird/learn-basics-get-started) pour en savoir plus sur son utilisation.

## Transmission

![Application - Transmission](transmission.png "Application - Transmission")

**Transmission** : le client bittorrent. Utilisez-le pour télécharger des films, des fichiers volumineux ainsi que des images de logiciels en utilisant le tiers de la technologie la plus utilisée sur Internet, BitTorrent, qui est plus rapide et plus fiable que le téléchargement normal. La transmission peut également être utilisée pour télécharger des fichiers directement vers une personne disposant de la même technologie sur Internet. 

## Vidéos

![Application - Vidéos](mediaplayer-app.png "Application - Vidéos")

**Vidéos** : le lecteur vidéo (également connu sous le nom de **Totem**) prend en charge la lecture de fichiers audio et vidéo avec des fonctionnalités de sous-titres, de répétition et d'amélioration de l'affichage. Étant un lecteur moderne, il prend également en charge la lecture de vidéos depuis Internet.

**Note** : il sera bientôt remplacé par **Showtime**, application plus moderne et plus conforme aux standards **GNOME**.



