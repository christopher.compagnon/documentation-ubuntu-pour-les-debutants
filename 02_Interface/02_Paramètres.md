# Les paramètres

Les paramètres permettent d'accéder au paramétrage du comportement de votre système d'exploitation et de certains logiciels.
Son icône est aisément identifiable par :

![Paramètres - icône](system-settings.png "Paramètres - icône")

Les paramètres sont accessibles de plusieurs façons :
- depuis les applications : cliquer sur le bouton « Afficher les applications » et sélectionner l'application depuis son icône ou bien depuis la zone de recherche, en tapant les premières lettres du mot « paramètres »;
- depuis le menu système : cliquer sur le menu système (en haut à droite de l'écran, le même qui sert à éteindre l'ordinateur) et choisissez le menu « paramètres » ou l'icône correspondante.

Cette application ouverte rassemble tous les paramètres. Le menu à gauche donne accès au différentes sections suivantes.

**Astuce** : vous pouvez ajouter l'icône dans vos favoris en cliquant dessus (clic droit) et en sélectionnant « Éplingler au Dash ». L'icône sera alors toujours visible et accessible depuis là.

## Wi-Fi

Cette partie n'est active que lorsqu'un périphérique Wi-Fi est installé.

## Réseau


## Bluetooth


## Arrière-plan

![Paramètres - Fond d'écran](Ubuntu_Parametres_wallpaper.png "Paramètres - Fond d'écran")

Pour changer de fond d'écran et choisir parmi ceux proposés.
Vous pouvez aussi ajouter votre propre image à partir du bouton «*Ajouter une image*» dans la barre de titre à droite.

## Apparence

L'apparence générale d'**Ubuntu** se règle ici.

![Paramètres - multi-écrans](Ubuntu_Parametres_Apparence.png "Paramètres - multi-écrans")

Vous pouvez ainsi :

- choisir le thème clair ou le thème sombre, ainsi que différentes variantes de couleurs.
- La taille des icônes.
- la position des nouvelles icônes.
- l'affichage du dossier personnel sur le bureau (actif par défaut) qui vous permet d'ouvrir votre répertoire personnel (*HOME*) en cliquant dessus.
- modifier le comportement et la position du *Dock* (à gauche par défaut). Si on choisit de le positionner en bas, alors :

![Paramètres - Dock en bas](Ubuntu_Parametres_Apparence_dockbas.png "Paramètres - Dock en bas")

on peut aussi choisir les icônes à y afficher (**Configurer le comportement du Dock >**).

![Paramètres - Dock](Ubuntu_Parametres_Apparence_Dock.png "Paramètres - Dock")

Pour un réglage plus précis de l'apparence, Utilisez **Gnome Tweaks**.

## Notifications


## Recherche

## Multi-tâches

![Paramètres - Multi-tâches](Parametres_multitaches.png "Paramètres - Multi-tâches")

Cette section permet de paramétrer l'interaction entre l'espace de travail et son utilisateur, comme les coins actifs, le redimensionnement en glissant les fenêtres, la gestion des bureaux virtuels dynamiques, …

## Applications


## Confidentialité

Cette partie permet d'accéder à des configurations liées à la confidentialité comme :
* La connectivité
* Le verrouillage de l'écran
* Les services de localisation
* Thunderbolt
* L'historique des fichiers et corbeille
* Les diagnostics

### Connectivité

![Paramètres - Confidentialité  - Connectivité](Parametres_Confidentialite_connectivite.png "Paramètres - Confidentialité  - Connectivité")

### Écran

Permet de déterminer le déclanchement du verrouillage automatique de la session et la mise en veille afin d'éviter au mieux qu'un individu n'accède à votre ordinateur en cas d'absence.

![Paramètres - Confidentialité  - Écran](Parametres_Confidentialite_ecran.png "Paramètres - Confidentialité  - Écran")


Les options suivantes sont disponibles :
* **Délai de l'écran noir** : durée après laquelle l'affichage s'éteint et laisse un écran noir. Cette option peut être désactivée par le choix *Jamais*.
* **Verrouillage automatique de l'écran** :
* **Délai de verrouillage automatique de l'écran** : période après laquelle l'écran devient noir lorsque l'écran est automatiquement verrouillé. Choisir une durée. L'option *Extinction de l'écran* permet d'éteindre l'écran afin d'économiser de l'énergie.
* **Affichage des notifications sur l'écran verrouillé** : lorsque l'écran est verrouillé, les notifications peuvent s'afficher (arrivée d'un courriel, musique en cours d'écoute, …). Cette option désactivée peut donc être utile pour préserver votre intimité.
* **Verrouiller l'écran lors de la mise en veille** : lorsque la mise en veille est activée, le session est verrouillée. En cas de sortie de veille, il faudra donc entrer le mot de passe pour retourner dans la session.


### Services de localisation

Les services de localisation permettent à certaines applications de vous positionner automatiquement (météo, …). Cette option active autorise le partage de votre position géographique.

### Thunderbolt


### Historique des fichiers et corbeille


![Paramètres - Confidentialité  - Corbeille](Parametres_Confidentialite_corbeille.png "Paramètres - Confidentialité  - Corbeille")


### Diagnostics

Paramètre l'envoi des rapports de diagnostic à *Canonical* en cas de plantage d'une application. 

![Paramètres - Confidentialité  - Diagnostics](Parametres_Confidentialite_diagnostics.png "Paramètres - Confidentialité  - Diagnostics")

Trois options sont possibles :
* Automatique
* Manuelle
* Jamais

Cette option se trouve dans la partie **Confidentialité** car elle est liée à votre usage des logiciels. Vous pouvez considérer que ces informations ne concernent pas *Canonical*.

## Comptes en ligne

Permet de définir vos comptes en ligne afin de s'y connecter et d'intégrer les services à votre ordinateur.

Les services disponibles sont :
- **Google** : accès à un compte **Google** afin de synchroniser les courriels, les agendas, etc.
- **Nextcloud** : accès à un compte [Nextcloud](https://fr.wikipedia.org/wiki/Nextcloud), un logiciel libre de site d'hébergement de fichiers et une plateforme de collaboration. Il est possible d'héberger sa propre instance chez soi.
- **Microsoft** : pour se connecter à la messagerie en ligne [outlook.com](https://outlook.live.com/owa/) fournie par **Microsoft**.
- **Microsoft Exchange** : le service de messagerie professionnel de **Microsoft**.
- **Last.fm** : le service de partage et de découverte de musique [Last.fm](https://www.last.fm/fr/).
- **IMAP et SMTP** : pour définir un compte de messagerie générique chez un fournisseur autre que **Google** ou **Microsoft**.
- **Enterprise login (Kerberos)** : pour les accès sécurisés aux réseaux d'entreprise par authentification **Kerberos**.


## Partage

Permet de paramétrer l'accès au bureau et aux disque locaux.

- **Bureau distant** : permet d'accéder à distance (edpuis un autre ordinateur) au travers d'un logiciel de connexion à distance comme *Remmina* (installé par défaut sur Ubuntu).
- **Partage de médias** : permet de partage les disques locaux (c'est-à-dire présents sur l'ordinateur local) avec des personnes présentes sur le réseau.



## Son

Le volume sonore du système global et de chaque application se règle ici.

![Paramètres - Son](Ubuntu_Parametres_Son.png "Paramètres - Son")

Vous pouvez aussi choisir le périphérique d'entrée (microphone) ou sortie (casque/haut-parleur) si vous en avez plusieurs.

**Note** : comme il peut être fastideux de changer de périphérique en passant à chaque fois par **Paramètres > Son**, il existe des extensions qui permettent de basculer plus aisément depuis la zone de statut.

## Énergie

![Paramètres - Énergie](Ubuntu_Parametres_Energie.png "Paramètres - Énergie")

Dans cette partie, vous pouvez régler l'usage des performances de la machine, afin d'allonger la durée de vie de la batterie si vous êtes sur un ordinateur portable.

En fonction de la machine dont vous disposez, vous pouvez avoir deux ou trois **Mode d'énergie** disponibles.

![Paramètres - Énergie](Ubuntu_Parametres_Energie_2.png "Paramètres - Énergie")

Vous pouvez aussi régler la durée de mise en veille.

## Écrans

La gestion des écrans, leur définition et leur orientation se règle ici.

![Paramètres - Écrans](Ubuntu_Parametres_Ecrans_Choix.png "Paramètres - Écrans")

Les paramètres disponibles :

- **Orientation** : portrait (écran vertical) ou paysage (écran horizontal).
- **Définition** : résolution de l'écran affiché. En cliquant dessus vous accédez aux résolutions possibles. Choisissez celle qui vous convient le mieux.
- **Fréquence de rafraîchissement** : cette option est normalement déterminée automatiquement pas le système d'exploitation. Sauf si vous savez ce que vous faites, laissez la valeur par défaut.
- **Mise à l'échelle fractionnaire** : La mise à l'échelle fractionnaire est un moyen de mettre à l'échelle vos icônes, vos fenêtres d'application et votre texte afin qu'ils n'apparaissent pas écrasés dans un affichage haute résolution (aussi appelé HiDPI). Vous pouvez jouer avec afin de savoir ce qui vous convient le mieux.

Pour toute modification, **Ubuntu** vous affiche le résulat à la validation et vous demande si vous voulez conserver les nouveaux paramètres.

![Paramètres - Écrans validation](Ubuntu_Parametres_Ecran_validation.png "Paramètres - Écrans validation")

Si la validation ne s'effectue pas dans un laps de temps de 20 secondes, **Ubuntu** considère qu'il y a un problème et revient à la configuration d'origine.

### Cas de plusieurs écrans

**Ubuntu** sait aussi gérer les multi-écrans, c'est-à-dire plusieurs écrans installés sur la même machine.
Dans ce cas, il affichera dans cette section tous les écrans connectés.

![Paramètres - multi-écrans](Ubuntu_Parametres_Multiecrans.png "Paramètres - multi-écrans")

Vous pourrez les paramétrer individuellement. Il suffit de sélectionner l'écran en question pour lui appliquer les modifications.

#### Déplacement d'un écran

Si l'écran est mal positionné par rapport à la configuration proposée, il suffit de cliquer dessus et de le glisser au bon endroit. Ainsi, si l'écran est physiquement placé à droite mais que son positionnement configuré est à gauche, il suffit de sélectionner l'écran dans le paramétrage est de le glisser au bon endroit (donc à droite) pour que la configuration de l'ordinateur corresponde à celle de la réalité physique.

Ainsi déplacer une fenêtre d'un écran à l'autre s'effectuera dans un sens plus intuitif.

#### Définition de l'écran principal

De la même façon, le premier écran connecté est considéré comme l'écran principal par défaut. Dans le paramétrage, il s'agit de l'écran représenté avec un bord en haut plus épais. C'est sur cet écran que s'affichera la barre des tâches. Cette configuration peut être changée en sélectionnant le bon écran depuis la zone **Écran principal**.

Après la modification, si la validation ne s'effectue pas dans un laps de temps de 20 secondes, **Ubuntu** considère qu'il y a un problème et revient à la configuration d'origine.

## Souris et pavé tactile

![Paramètres - Souris](Ubuntu_Parametres_Souris.png "Paramètres - Souris")

Dans cette partie, vous réglez le bouton principal de la souris ou du pavé tactile ainsi que la vitesse de défilement.

Si vous avez un pavé tactile ou que vous en branchez un, une nouvelle section apparaît :

![Paramètres - Pavé tactile](Parametres-pave_tactile.png "Paramètres - Pavé tactile")

Depuis cette zone, vous pouvez configurer un certain nombre de comportements :

- **Pavé tactile** : activer/désactiver l'usage du pavé tactile;
- **Défilement naturel** : le défilement déplace le contenu, pas la vue;
- **Vitesse du pavé tactile** : réglage de la sensibilité/vitesse à laquelle l'utilisateur effectue le double-clic;
- **Taper pour cliquer** : le clic est simulé en tapant sur la zone tactile, une fois pour un clic, deux fois pour un double-clic;
- **Défilement à deux doigts** : utilise deux doigts simultanément pour effectuer le défilement vertical et horizontal;
- **Défilement aux bords** : le défilement s'effectue sur les bords du pavé tactile.

Vous pouvez tester les paramètres en cliquant sur le bouton «*Tester vos paramètres*» grâce à un petit jeu.

![Paramètres - Tester les paramètres](Ubuntu_Parametres_Souris_2.png "Paramètres - Tester les paramètres")


## Clavier

La disposition du clavier et l'ajout de nouvelles dispositions se règle ici.

![Paramètres - Clavier](Ubuntu_Parametres_Clavier.png "Paramètres - Clavier")

Vous pouvez ajouter d'autres dispositions de claviers depuis la zone «*Sources de saise*», en cliquant sur le plus et en choisissant dans la liste le nouveau clavier à ajouter.

![Paramètres - Nouveau Clavier](Ubuntu_nouveau_clavier.png "Paramètres - Nouveau Clavier")

Pour accéder à la liste complète, cliquez sur le dernier bouton surmonté des 3 points.

Cette option peut être utile lorsque vous désirez un clavier avec une autre disposition que celle configurée à l'installation, ou bien si vous écrivez dans plusieurs langues et que vous désirez passez de l'un à l'autre aisément.

Lorsque plusieurs claviers sont configurés, une notification apparaît dans la barre de menus :

![Bureau - langues Clavier](Ubuntu_Bureau_langues.png "Bureau - langues Clavier")


### Combinaisons de touches

Les combinaisons de touches sont définies et modifiables ici.


## Imprimantes

Lorsqu'elles sont connectées à votre machine (USB) ou à votre réseau (Wi-Fi, …), les imprimantes apparaissent ici. Il est aussi possible de les ajouter manuellement en cas de problème.
Pour plus de détails, veuillez vous rendre au chapitre [Impression](../05_Impression/Configuration.md).

## Médias amovibles


## Couleur


## Pays et langue


## Accessibilité


## Utilisateurs

Dans cette section, vous accédez aux utilisateurs définis sur la machine, à commencer par votre propre utilisateur.

![Paramètres - Utilisateurs](Ubuntu_Parametres_Utilisateurs.png "Paramètres - Utilisateurs")

Vous pouvez changer votre mot de passe, votre image, votre nom…


### Connexion par empreintes digitales

Si vous disposez d'un lecteur d'empreintes, c'est aussi dans cette section que vous pourrez l'activer grâce à l'option **Connexion par reconnaissance d'empreintes** :

![Paramètres - Lecteur d'empreintes](Ubuntu_Parametres_Lecteur_empreintes.png "Paramètres - Lecteur d'empreintes")

Dans laquelle vous définirez les empreintes que vous désirez utiliser.

![Paramètres - Lecteur d'empreintes](Ubuntu_Parametres_Lecteur_empreintes_2.png "Paramètres - Lecteur d'empreintes")

### Créer des utilisateurs

Si vous êtes administrateur, c'est aussi là que vous pourrez créer d'autres comptes pour d'autres personnes (ou pour vous, si vous désirez par exemple séparer des activités). Pour cela, il vous suffit de déverrouiller le bandeau en haut.

## Applications par défaut

Choix des applications à utiliser par défaut en fonction des types de fichiers ou de médias.

![Paramètres - Applications par défaut](Ubuntu_Parametres_Applications_defaut.png "Paramètres - Applications par défaut")

Vous pouvez vérifier ou changer ici l'application à lancer lorsque vous effectuez une opération par défaut (double-clic, …).

Vous pouvez définir :

- **Sites web** : navigateur Web pour surfer sur Internet.
- **Courriels** : logiciel de gestion des courriels.
- **Calendrier** : logiciel de gestion des calendriers. Si vous avez paramétré un compte **Google**, par exemple, vous pouvez gérer vos calendriers *Google* depuis cette application.
- **Musique** : logiciel pour lire la musique.
- **Vidéos** : logiciel pour lire les fichiers vidéos.
- **Photos** : logiciel pour lire les photos.

## Date et heure


## À propos

![Paramètres - À propos](Ubuntu_Parametres_apropos.png "Paramètres - À propos")

Informations sur le système installé :

- Nom de l'appareil : nom utilisé et visible sur le réseau. Vous pouvez le changer ici.
- Mémoire (RAM).
- Processeur.
- Carte graphique.
- Capacité du disque.
- Nom du système d'exploitation.
- Type de système d'exploitation (32 bits, 64 bits, …).
- Version de **GNOME**.
- Système de fenêtrage (**Wayland** à partir de la version *22.04*).
- Mises à jour logicielles : accès à **Logiciels et mises à jour**.
