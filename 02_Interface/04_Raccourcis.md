# Claviers et raccourcis

**À noter** : Les touches de clavier sont généralement représentées entre crochets. Par exemple **[ctrl]**, **[alt]**, etc.
Une combinaison de touches est représentée par un "+", par exemple **[crtl] + [alt]**, signifiant qu'il faut appuyer sur les touches **simultanément**.

## Le clavier

Les claviers peuvent avoir plusieurs dispositions, en fonction de la langue et de la position géographique.

Cette disposition est nommée en fonction des premières lettres du clavier.

### Azerty

Les francophones de *France* utilisent généralement le clavier *azerty*, disposé de la façon suivante :

![Le clavier français azerty](https://upload.wikimedia.org/wikipedia/commons/b/b9/KB_France.svg "Le clavier français azerty")

### Qwertz

Les francophones de *Suisse* et du *Luxembourg* utilisent généralement le clavier *qwertz*, disposé de la façon suivante :

![Le clavier francophone qwertz](https://upload.wikimedia.org/wikipedia/commons/0/0a/KB_Swiss_Luxemburg_text.svg "Le clavier francophone qwertz")

### Qwerty

Les francophones du *Canada* utilisent généralement le clavier *qwerty*, disposé de la façon suivante :

![Le clavier francophone qwerty](https://upload.wikimedia.org/wikipedia/commons/6/62/KB_Canadian_French.svg "Le clavier francophone qwerty")

### Bépo

Les francophones peuvent également utiliser le clavier *bépo*, optimalisé pour minimiser le déplacement des doigts et améliorer la vitesse de frappe, disposé de la façon suivante :

![Le clavier francophone bépo](https://upload.wikimedia.org/wikipedia/commons/6/6a/Carte-bepo-simple.svg "Le clavier francophone bépo")

## Ajouter/supprimer un clavier

Il est possible d'ajouter ou de supprimer un clavier.

Pour cela, se rendre dans **![Paramètres > Clavier](./02_Paramètres.md)**.

## Touches supplémentaires

La disposition du clavier indiquée ci-dessus décrit le cœur du clavier. En plus de cette disposition, il y a un certain nombre d'autres touches situées en haut et à droite, dont la disposition dépend du modèle et de la marque du clavier ou de l'ordinateur.

## Les touches spéciales

Quel que soit la disposition du clavier, il convient d'identifier les touches spéciales qui serviront par la suite.

### Apparence d'une touche

En observant la touche d'un clavier, vous remarquerez plusieurs signes sur une même touche.

Ils sont généralement au maximum de 3 par touche :
- le caractére principal situé *au centre* (en fait en bas à gauche sur les touches de la rangée numérique supérieure et en haut à gauche sur les touches normales du clavier), accessible directement, sans aucune combinaison;
- le caractère secondaire, situé généralement en haut à gauche sur les touches de la rangée numérique supérieure, sinon elle correspond à la même lettre en version majuscule, et accessible par la touche **[shift]/[maj] + [touche]**;
- le caractère tertiaire ou alternatif, situé en bas à droite, accessible par la combinaison **[alt gr] + [touche]**.

### Super/Windows

La touche **[Super]**, parfois nommée **[Windows]** pour les claviers prévus pour être utilisés avec le système d'exploitation **Windows**.

### **[ctrl]** (contrôle)

La touche de contrôle est identifiée sur le clavier par **ctrl**.
Il y a généralement deux touches **[ctrl]**, l'une située en bas à droite, l'autre en bas à gauche du clavier.


### **[alt]** (alternate/alterner)


### **[alt gr]** (alternate graphic/alterner caractère)

Abbrévitaion de *Alternate graphic*, la touche **[alt gr]** permet d'accéder aux versions alternatives de certains signes.


### shift/maj(uscule) : **[ &#x21E7; ]**

Généralement représentée par une flèche vers le haut **[ &#x21E7; ]**, ou parfois par la graphie *Maj.*, cette touche est disposée de part et d'autre du clavier et permet d'accéder aux majuscules ou aux caratères situés sur la partie supérieure de la touche.

## Les touches spécifiques

Il existe également des touches pour étendre les fonctionnalités du système d'exploitation, agissant alors comme des raccourcis vers des applications ou des combinaisons de touches.

|Touche|Action|
|:------------|:------------|
|**[ &#x21E5; ]** ou **[ &#x21B9; ]**|insèrer une tabulation|
|**[ &#x2B05; ]** ou **[ &#x25C0; ]**|déplacer vers la gauche (1 caractère)|
|**[ &#x27A1; ]** ou **[ &#x25B6; ]**|déplacer vers la droite (1 caractère)|
|**[ &#x2B06; ]** ou **[ &#x25B2; ]**|déplacer vers le haut (1 ligne)|
|**[ &#x2B07; ]** ou **[ &#x25BC; ]**|déplacer vers le bas (1 ligne)|
|**[ &#x2196; ]** ou **[ &#x21F1; ]** ou **[home]**|remonter au début de la ligne|
|**[ &#x2198; ]** ou **[ &#x21F2; ]** ou **[end]**|descendre en fin de ligne|
|**[ &#x21DE; ]** ou **[PgUp]** (pour *Page Up*)|remonter d'une page (c'est-à-dire la première ligne de texte visible à l'écran)|
|**[ &#x21DF; ]** ou **[PgDn]** (pour *Page Down*)|descendre d'une page (c'est-à-dire la dernière ligne de texte visible à l'écran)|
|**[Impr.écr]** (pour *Impression Écran*) ou **[PrtSrc]** (pour *Print Screen*)|prendre une copie écran, c'est-à-dire enregistrer sous la forme d'une image (enregistrée dans le répertoire *Images*) l'exacte réplique de l'écran dans son état actuel|
|**[Suppr]** (pour *Supprimer*) ou **[Del]** (pour *Delete*)|supprimer le caractère placé **après** le curseur|
|**[ &#x232B; ]**|supprimer le caractère placé **avant** le curseur|
|**[Échap]** (pour *Échappement*) ou **[Esc]** (pour *Escape*)|sortir de l'opération en cours|


## Les raccourcis clavier (par défaut)

Les raccourcis clavier correspondent à l'action simultanée de plusieurs touches afin d'effectuer une action.

**À noter** : Les raccourcis clavier sont définis dans **Paramètres > Clavier > Raccourcis clavier** et peuvent être modifiés.

|Combinaison de touches|Action|
|:------------|:------------|
|**[Super] + [ &#x2B06; ]**|*Maximiser* (c'est-à-dire : occupe tout l'écran) la fenêtre active|
|**[Super] + [ &#x27A1; ]**|Positionner la fenêtre active sur la moitié *droite* de l'écran|
|**[Super] + [ &#x2B05; ]**|Positionner la fenêtre active sur la moitié *gauche* de l'écran|
|**[Super] + [Shift] + [ &#x27A1; ]**|Positionner la fenêtre active sur l'écran à droite (dans le cas où plusieurs écrans sont connectés à l'ordinateur)|
|**[Super] + [Shift] + [ &#x2B05; ]**|Positionner la fenêtre active sur l'écran à gauche (dans le cas où plusieurs écrans sont connectés à l'ordinateur)|

