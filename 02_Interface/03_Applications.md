# Programmes, applications, fenêtres

## Qu'est-ce qu'un programme ?

Le système d’exploitation (Linux) ne sert qu’à exploiter les ressources de la machine (écran, clavier, disque dur, …). Par défaut, il ne fait pas grand-chose. Afin d’étendre ses capacités et le rendre utilisable au commun des mortels, il exécute des programmes, ces derniers exécutant les tâches utiles à l’utilisateur.

Ces programmes ont beaucoup évolué avec le temps et sont devenus de plus en plus complexes. Les interfaces graphiques ont rendu cette complexité plus transparente.

Le **programme** désigne un programme ce qui s’exécute sur l’ordinateur et n’*effectue qu’une et une seule tâche*. Ce programme est composé d’un ensemble d’instructions prédéfinies (donc programmées) pour demander à l’ordinateur d’effectuer la tâche.

## Qu'est-ce qu'une application ?

Une **application** désigne un programme plus complexe qui exécute un ensemble de tâches (ou fonctionnalités) dont l’objectif est de remplir au mieux, de façon efficace et agréable, l’objectif de l’application.

**Par exemple** : l’objectif d’un navigateur Web est d’afficher une page Web. C’est son objectif principal. Cependant, pour le rendre plus agréable à l’usage, sans forcer l’utilisateur à exécuter tout un tas de commandes complémentaires, le navigateur Web inclut d’autres fonctionnalités comme l’impression d’un document, la gestion des mots de passe, l’enregistrement d’un fichier, la gestion de l’historique de navigation, …

Certaines de ces fonctionnalités utilisent celles du système d’exploitation auxquelles l’application fait appel, d’autres sont incluses dans l’application elle-même.

Même si les deux termes ont des significations différentes pour les puristes, dans la grande majorité des cas, les termes «programme» et «application» sont interchangeables.

Aujourd’hui, avec le développement des interfaces graphiques qui rendent l’usage plus simple, les applications sont identifiables par des noms ou des icônes cliquables.

De façon commune : 1 application = 1 icône.

Sous **Ubuntu**, les applications sont disponibles par défaut sous la forme d'icônes. Les applications sont affichées en cliquant sur le bouton «Afficher les applications» dans la barre des tâches :

![Application - icônes de redimensionnement](Afficher_les_applications.png "Application - icônes de redimensionnement")

## La fenêtre

Avec le développement des interfaces graphiques, les applications en cours d'exécution sont matérialisées par des fenêtres. Ces fenêtres sont d'ailleurs à l'origine du nom du système d'exploitation **Microsoft Windows**.

Le rôle de la fenêtre est de rendre l'interaction avec l'utilisateur plus simple, plus visuelle, plus intuitive, plus naturelle. En plus de proposer une interface intuitive, la fenêtre est avant tout un élément flottant qui peut être déplacé sur l’écran, au bon vouloir de l’utilisateur. Ce dernier peut ainsi adapter son «bureau» à sa façon de travailler et de concevoir son espace.

Généralement, une fenêtre correspond à une fonctionnalité (mais pas toujours). Une application ayant plusieurs fonctionnalités, elle peut donc ouvrir plusieurs fenêtres en fonction de la tâche à exécuter. C'est ce qui se passe, par exemple, lorsqu'on enregistre un fichier, une nouvelle fenêtre s'ouvre pour demander où poser le document. Lorsqu'elle s'exécute, une application a, au minimum, une fenêtre principale.

Excepté quelques exceptions, les fenêtres sont tracées et gérées par le système d’exploitation.

**Remarque** : en réalité, ce n’est pas tout à fait exact, mais considérons que cette assertion est vraie pour simplifier les explications.

Comme le système d’exploitation gère l’affichage des fenêtres, il donne à toutes les applications le même rendu ce qui permet de disposer d’une homogénéité en fonctionnalités et en apparence.

Une fenêtre possède ainsi plusieurs éléments qu'on retrouve d'une application à une autre.

## La barre de titre 

Située en haut, originellement avec le titre (généralement le nom de l’application et le document ouvert) au milieu. Si cette assertion était vraie jadis, elle est aujourd'hui de moins en moins, de plus en plus d'applications utilisant cet espace pour améliorer la productivité. Par exemple, *Firefox* utilise cette zone pour la barre d'adresse et/ou de recherche. Le *Gestionnaire de fichiers* pour ajouter des options et des raccourcis, etc.

Sous **Ubuntu**, le nom de l'application apparaît aussi dans la barre des activités (la barre horizontale en haut de l'écran). Depuis cette barre, il est possible d'effectuer des actions, comme quitter, accéder au autres fenêtres ouvertes ou ouvrir une nouvelle fenêtre.

## Les icônes de redimensionnement

Au niveau de la barre de titre (ou de ce qui était jadis la barre de titre), les fenêtres possèdent généralement 3 boutons de redimensionnement sour la forme d'icônes situés sur la barre de titre et qui se situent en haut à gauche ou à droite en fonction des systèmes d’exploitation ou de la configuration. Sous Linux, tout étant paramétrable, il est possible de les afficher à droite ou à gauche, mais aussi de n’en afficher que certains ou aucun.

Sous **Ubuntu**, par défaut, ces icônes se situent à droite :

![Application - icônes de redimensionnement](Application_icones.png "Application - icônes de redimensionnement")

Ici, de gauche à droite : **minimiser**, **agrandir/réduire** et **fermer**.

**Remarque** : certaines fenêtres de dialogue ne disposent pas de tous les boutons de redimensionnement. Généralement, une fenêtre dispose au moins un bouton pour la fermer. Parfois, certaines fenêtres de dialogue (comme la fenêtre de dialogue d'impression) ne disposent ni de barre de titre, ni de boutons de redimensionnement, mais il existe un bouton «Fermer» ou «Annuler» qui permet de fermer la fenêtre. Les fenêtres de dialogues n'apparaissent pas dans la barre des activités car elles sont des fenêtres secondaires.

### Agrandir/réduire

Ce bouton activé étend la fenêtre à toute la surface du bureau. La désactivation de ce bouton ramène la fenêtre à sa position/dimension antérieure.

Cette fonctionnalité peut aussi être obtenue en double-cliquant sur la barre de titre de la fenêtre. Sous **Ubuntu**, le déplacement de la fenêtre vers le haut du bureau permet d’obtenir le même effet.

Le redimensionnement d'une fenêtre dispose de raccourcis :

### Raccourci

En double-cliquant sur la barre de titre, la fenêtre s'agrandit pour occuper tout l'espace. Le même résultat est obtenu en glissant la fenêtre vers le haut de l'écran.

En double-cliquant à nouveau sur la barre de titre, la fenêtre reprend sa taille précédente.

### Minimiser

Ce bouton masque totalement la fenêtre. L’application est toujours exécutée, mais elle attend en arrière-plan, de façon non visible à l’utilisateur. Seule persiste son icône, généralement dans la barre des tâches, sur laquelle il faut cliquer pour rappeler l’application sur le bureau.

### Fermer

Ce bouton permet de fermer l’application. Son exécution s’arrête et les ressources (processeur, mémoire) sont alors libérées.
La fermeture d’une application peut aussi être obtenue en passant par le menu adéquat dont la disposition dépend de l’application. Il est aussi possible de fermer une application depuis la barre des activités.

**Attention** : dans certaines applications (comme un navigateur Web), la fermeture de tous les onglets entraîne la fermeture de l’application par voie de conséquence.

### Autres boutons

Certains applications, comme le gestionnaire de fichiers, possède d'autres boutons positionnés au même endroit, afin d'accéder à d'autres options.

![Gestionnaire de fichiers - autres options](Gestionnaire_de_fichiers_icones_fenetre.png "Gestionnaire de fichiers - autres options")

## Redimensionnement d'une fenêtre

Une fenêtre possède généralement des bords redimensionnables qu’on peut saisir (saisir = clic gauche) avec la souris (ou autre matériel de pointage comme un pavé tactile) afin de contracter ou étendre sa taille.

Tout ce qui se situe à l’intérieur de cette fenêtre est propre à l’application elle-même, même si l’affichage tend à se normaliser pour plus d’homogénéité.

Ainsi, nous retrouverons des menus déroulants pour accéder aux différentes fonctionnalités de l’application : «ouvrir», «enregistrer», «enregistrer sous», «imprimer», «quitter», etc.


## Afficher une fenêtre

Sous **Ubuntu**, une application lancée apparaît avec un point sous son icône. Lorsque l'application est lancée plusieurs fois (on dit aussi «lorsqu'il y a plusieurs *instances*»), il y a autant de points que l'application a été lancée de fois. Chaque point matérialise une fenêtre différente qu'il est possible d'afficher.

![Barre des tâches - applications lancées](Applications_barre_des_taches.png "Barre des tâches - applications lancées")

Il est possible d'afficher les fenêtres en miniatures depuis la barre des tâches, avec un clic droit sur l'icône de l'application :

![Barre des tâches - liste des instances d'une application](Applications_acces_fenetres.png "Barre des tâches - liste des instances d'une application")

puis en cliquant sur **Toutes les fenêtres >** :

![Barre des tâches - liste des fenêtres d'une application](Applications_liste_fenetres.png "Barre des tâches - liste des fenêtres d'une application")

Les différentes fenêtres apparaissent sous la forme de miniatures.

On peut alors afficher la fenêtre en question en cliquant sur la miniatune correspondante. Il est aussi possible d'effectuer la même opération depuis la barre des activités.

## Pour résumer

- Chaque application affiche au moins une fenêtre;
- Chaque fenêtre *principale* dispose en haut d'une barre (nommée barre de titre) avec des boutons de redimensonnement;
- Les fenêtre secondaires (comme les fenêtres de dialogue) ne disposent pas toujours d'un titre ou de boutons de redimensionnement mais d'un bouton «Fermer» ou «Annuler»;
- Chaque fenêtre (exceptées quelques fenêtres de dialogue) peut être déplacée, réduite, redimensionnée, fermée;
- Une application peut être fermée depuis ses boutons de redimensionnement, la barre des tâches (clic droit sur l'icône de l'application puis «Quitter»), la barre des activités (clic gauche sur le nom de l'application puis «Quitter»), ou le menu standard (*Fichier > Quitter*).
