# Gérer l'affichage des écrans haute définition

Les écrans modernes atteignent des définitions sans commune mesure. Si l'écran HD (haute définition : 1980 x 1080 pixels) était une définition de luxe il y a encore quelques années, le 4K tend à le remplacer progressivement. 4K, c'est 4 fois la surface d'affichage d'un écran HD. Devant de telles définitons, nous pouvons nous sentir petits ou la police affichée peut, elle, être trop petite pour nos yeux.

Il convient de l'ajuster de façon adéquate.

**Ubuntu** propose différents réglages.

## Afficher les grandes polices

La première chose à vérifier est donc l'usage des polices de grande taille. Pour cela, se rendre dans **Paramètres > Accessibilité** et sélectionner **Image > Grand texte**.

![Paramètres Accessibilité Grand texte](Accessibilite_Grand_texte.png "Paramètres Accessibilité Grand texte")


## Mise à l'échelle

L'affichage, par défaut à 100% – soit un rapport d'échelle de 1/1 –, peut être modifié pour «zoomer» en augmentant le rapport d'échelle.

Ce dernier est généralement une valeur entière – 200% soit 2/1, 300% soit 3/1, etc. – mais peut aussi être fractionnaire – 125%, 150%, etc. – si l'option **Mise à l'échelle fractionnaire** est activée.

![Paramètres Écrans Échelle](Parametres_ecrans_echelle.png "Paramètres Écrans Échelle")


## Agmenter la taille des polices du système

Changer la taille des polices nécessite d'avoir installé **Gnome Tweak Tool**.

Rendez-vous donc dans **Gnome Tweak Tool > Polices** et ajustez l'affichage depuis l'option **Facteur de mise à l'échelle**. Augmentez la valeur jusqu'à obtenir un affichage lisible et reposant.

![Tweaks Polices Échelle](Tweaks_Polices_affichage.png "Tweaks Polices Échelle")

**Remarque** : il est aussi possible de changer directement la taille des polices et d'en choisir de plus grandes, mais le facteur d'échelle reste la méthode la plus aisée à appliquer car elle conserver l'harmonie choisie au départ par les ergonomes.

## Augmenter le rapport d'affichage de votre navigateur Web (Firefox)

Le navigateur propose généralement un affichage indépendant du système. Il faut donc effectuer le réglage à la main.

Depuis votre navigateur Web (Firefox), rendez-vous dans **Paramètres > Général**, section **Zoom**, et augmentez la valeur jusqu'à obtenir un affichage lisible et reposant.

![Firefox Général Zoom](Firefox_display_zoom.png "Firefox Général Zoom")

