# Le choix Ubuntu

**Ubuntu** est un système d'exploitation reposant sur **Linux**.

**Linux**, libre et open source, est un noyau dérivé d'[Unix](https://fr.wikipedia.org/wiki/Unix) (un des plus ancien systèmes d'exploitation réputé pour sa robustesse) developpé en 1991 par [Linus Torvalds](https://fr.wikipedia.org/wiki/Linus_Torvalds) alors qu'il est étudiant et cherche à tirer pleinement parti des fonctionnalités de son ordinateur personnel. 

*Linux* : Ce nom est inspiré à la fois de son parent, *Unix*, et de son créateur, *Linus*, et dont l'orthographe finale permet de définir que *Linux n'est pas Unix* (**L**inux **I**s **N**ot **U**ni**X**), jeu de mots sous forme d'acronyme récursif que nous finirons par retrouver à peu près systématiquement dans la communaunté du logiciel libre depuis que [Richard Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman) a créé le projet *GNU* (un autre système d'exploitation libre hérité d'Unix) en 1983 et qu'il a défini comme *GNU N'est pas Unix* (**G**NU **I**s **N**ot **U**nix).

Nous désignons à tort *Linux* comme étant le système d'exploitation complet alors qu'il n'est que son noyau.

Le système d'exploitation repose sur le noyau et un ensemble d'autres logiciels tout autour (comme l'interface graphique) afin de rendre la machine plus facile à utiliser.

Pour cette raison, vous verrez aussi *GNU/Linux* ou, pour les puristes, *GNU+Linux* afin de bien insister sur le fait que *Linux* (le noyau) est adossé à d'autres logiciels hérités du projet **GNU**.

Ainsi, selon la philosophie du logiciel libre, chacun est libre de contribuer à *Linux*, de faire une copie pour lui, de la modifier et de la distribuer.

Comme chacun est libre de contribuer, il existe tout un tas de logiciels, parfois effectuant les mêmes tâches que d'autres, ce qui implique un très grand nombre de possiblités de les associer ensemble pour former un système d'exploitation, que cela soit par l'interface proposée ou les outils pour le configurer et l'utiliser.
Ce choix d'un tout cohérent est appelé **distribution**.

**Ubuntu** est une des nombreuses distributions Linux disponibles. Il est distribué par la société **Canonical**.

## Pourquoi choisir GNU/Linux ?

Linux étant libre, son code source est accessible par tout le monde. Cette ouverture induit un certan nombre de conséquences :

- **Sécurité** : chacun peut identifier une faille de sécurité et la combler. Lorsqu'une faille est identifiée, Linux est d'ailleurs un des premiers systèmes d'exploitation à disposer d'un correctif. 
- **Modernité** : la rapidité à corriger les failles de sécurité s'applique aussi pour tout le reste. Les logiciels proposés sont généralement à la pointe, chaque distribution permettant de faire une proposition nouvelle, là où les systèmes d'exploitation traditionnels grand public doivent assurer une stabilité et, très souvent, une obsolescence plus rapide.
- **Intimité** : le code pouvant être audité pour vérifier qu'il ne contienne ni porte dérobée ni outil de surveillance, il garantit le respect de l'intimité de ses usagers.
- **Gratuité** : n'importe qui pouvant prendre le code et le compiler, n'importe qui peut construire son propre système d'exploitation, ce qui implique que la plupart des distributions sont gratuites, ce qui permet à tout le monde, même aux plus pauvres, de disposer d'un système d'exploitation moderne et parfaitement fonctionnel, sans limitations. Bien entendu, il est toujours possible (et même conseillé) de rétribuer à la hauteur de vos moyens les développeurs de vos outils favoris.
- **Extensivité** : le code étant ouvert et modifiable, n'importe qui peut ajouter les fonctionnalités qui lui sont nécessaires, de la façon qui lui convient le mieux.
- **Personnalisation** : l'extensivité permet non seulement d'ajouter mais aussi de retirer ce qui n'est pas nécessaire, de changer tout ce qui ne plaît pas, induisant alors une personnalisation qui rend le système d'exploitation unique, signature de son utilisateur, là où les systèmes d'exploitation traditionnels grand public ne proposent qu'un faible éventail de modifications possibles.
- **Performance** : l'audit du code source, sa modification et sa personnalisattion permettent de produire un code efficace qui induira des logiciels performants. Dans bien des cas, le système d'exploitation *Linux* est plus rapide que ses concurrents. C'est en grande partie pour cette raison qu'il est utilisé sur les supercalculateurs et les téléphones mobiles.
- **Portabilité** : *Linux* est utilisable sur un large éventail de matériel comme les serveurs, les téléphones, mais aussi les voitures, l'[Internet des Objets](https://fr.wikipedia.org/wiki/Internet_des_objets)… et, bien entendu, les vieux ordinateurs qui pourront alors retrouver une seconde jeunesse et être recyclés.

**Remarque** : comme très souvent, une qualité pouvant aussi être un défaut, *Linux* n'échappe pas à la règle. La sécurité pour être efficace, nécessite une connaissance technique qui la rend intrinsèquement plus difficile à appréhender, notamment lorsque le produit est personnalisable et extensible. Cette extensibilité a induit de nombreuses distributions, elles-mêmes provoquant un [paradoxe de l'abondance](https://fr.wikipedia.org/wiki/Paradoxe_de_l%27abondance) face auquel les utilisateurs, même les plus confirmés, restent hésitants, ne sachant pas quelle solution choisir.

Pour toutes ses qualités, *Linux* est devenu *LE* système d'exploitation le plus populaire au monde puisqu'il motorise [les principaux supercalculateurs](https://www.top500.org/statistics/list/), mais aussi **[Android](https://fr.wikipedia.org/wiki/Android)**, le système d'exploitation de *Google*, représentant à lui seul [les 3/4 du marché du mobile en 2020](https://gs.statcounter.com/os-market-share/mobile/worldwide).

Malgré tous ces avantages et ses succès, *Linux* reste très peu utilisé sur la machine de bureau du fait d'une mauvaise réputation qu'il traîne depuis l'époque où il fallait posséder des connaissances pointues en informatique pour l'installer et où le matériel n'était pas toujours compatible du fait que les constructeurs ne donnaient pas accès aux spécifications, ne fournissaient pas de pilotes ou de logiciels. La compatibilité était alors assurée par des passionnés, suite à un énorme travail de rétro-ingénierie, une longue succession d'essais et d'erreurs.

Cependant, ce n'est plus le cas aujourd'hui. Bien que peu utilisé au regard des autres systèmes d'exploitation, *Linux* n'est plus boudé par le public et les professionnels depuis l'arrivée de poids lourds sur le marché comme **Red Hat**, qui a fait de *Linux* son produit phare et, bien entendu, **Canonical**, qui fournissent ainsi (moyennant rétribution) le support nécessaire aux utilisateurs, filet de sécurité bienvenu en cas de problème.

Aujourd'hui, *Linux* est un système d'exploitation mature, pris au sérieux, stable et performant.

## Les distributions

Le noyau et les programmes mis à disposition étant libres, les utilisateurs ont rapidement créés leurs propres systèmes d'exploitation en fonction de leurs goûts et de leurs usages/besoins. Certaines distributions incluent une philosophie ou un contrat social, comme c'est le cas pour **Debian**.

Ces systèmes d'exploitation empaquetés et proposés comme un tout sont nommés *distributions*.

Ils comprennent généralement :
- le noyau *Linux*;
- une interface graphique;
- des logiciels.

**Ubuntu** est une des distributions *Linux* disponibles aujourd'hui mais il en existe beaucoup d'autres.

## Pourquoi choisir Ubuntu ?

Nous l'avons évoqué, un produit est d'autant plus populaire lorsqu'une entreprise solide garantit son support vers lequel les utilisateurs pourront se tourner en cas de problème, tant en assurant la stabilité et la pérennité.

Tout comme **Red Hat** avec la distribution du même nom, *Canonical* est l'entreprise derrière **Ubuntu** dont l'objectif premier était de proposer Linux comme sytème d'exploitation alternatif à *Microsoft* **Windows**. Pour avoir une chance, il fallait que cette distribution soit simple à utiliser.

**Ubuntu** est donc une des distributions à la fois facile à installer et configurer, mais aussi à prendre en main, tout en étant moderne, performant et stable.
Il est hérité d'une autre distribution, **Debian**, dont l'objectif est de fournir un système d'exploitation lui-même robuste et stable, mais avec pour conséquence d'inclure des logiciels assez anciens par rapport au marché. C'est parce que *Linux* souffrait de cette réputation d'avoir toujours *un train de retard* que des projets comme **Ubuntu** sont nés.

Parce qu'elle est créée et supportée par une entreprise, **Ubuntu** est une distribution commerciale. Même si le mot «commercial» fait grincer des dents les libristes (c'est ainsi qu'on appelle les défenseurs du logiciel libre), sans ces distributions commerciales, *Linux* n'aurait pas eu le succès qu'on lui connaît aujourd'hui. De plus, *Mark Shuttleworth*, le fondateur de **Canonical**, a prévu une [fondation **Ubuntu**](https://fr.wikipedia.org/wiki/Ubuntu_Foundation) pour reprendre et diriger le projet si *Canonical* venait à mettre la clef sous la porte et assurer ainsi la pérennité du produit. 

Une des qualités premières de *Linux* étant la personnalisation, il y a de fortes chances que cette distribution ne vous corresponde pas parfaitement. Cependant, par sa simplicité d'usage, elle vous permettra de mettre le pied à l'étrier, d'appréhender un système d'exploitation *Linux* et de comprendre ce que vous aimez ou n'aimez pas pour choisir, en définitive, la distribution la plus adaptée à vos besoins et à votre personnalité.

## Les différentes saveurs

Une même distribution peut être proposée avec une interface graphique différente de celle prévue au départ, le reste étant à peu près identique.
Cette variante est appelée *saveur*.

Pour **Ubuntu**, il existe différentes saveurs :
- **[Ubuntu](https://ubuntu.com/)** : la saveur naturelle utilisant l'interface graphique **Gnome**;
- **[Kubuntu](https://kubuntu.org/)** : utilisant l'interface graphique **KDE**;
- **[Ubuntu Mate](https://ubuntu-mate.org/)** : utilisant l'interface graphique **Mate**;
- **[Xubuntu](https://xubuntu.org/)** : utilisant l'interface graphique **Xfce**;
- **[Lubuntu](https://lubuntu.net/)** : utilisant l'interface graphique **Lxde**;
- **[Ubuntu DDE](https://ubuntudde.com/)** : utilisant l'interface graphique **Deepin Desktop Environment**, l'environnement de bureau développé pour **[Deepin Linux](https://www.deepin.org/en/)**, distribution fournie par *Wuhan Deepin Technology Co.*;
- **[Ubuntu Budgie](https://ubuntubudgie.org/)** : utilisant l'interface graphique **Budgie**, l'environnement de bureau développé pour **[Solus OS](https://getsol.us/home/)**, une autre distribution *Linux*;
- **[Ubuntu Kylin](https://www.ubuntukylin.com/index-en.html)** : variante **Ubuntu** initialement destinée à la Chine.

Il existe aussi :
- **[Ubuntu Studio](https://ubuntustudio.org/)** : une distribution spécialement conçue pour la production audio, vidéo, sonore et graphique. Cette distribution utilise l'interface graphique **KDE** ou **Xfce**, un noyau optimalisé (latence faible) et un ensemble de logiciels adaptés.


## Les autres distributions fondées sur *Ubuntu*

Chacun étant libre de prendre le code source, de le modifier pour le redistribuer, il existe des distributions créées à partir de **Ubuntu**.
Parmi celles-ci, nous pouvons citer :

- **Linux Mint** : 
- **Zorin OS** : 
- **Elementary OS** :
- **Trisquel** : distribution épurée des paquets non-libres.


## Les machines vendues avec *Ubuntu* (ou *Linux*)

*Linux* devenant de plus en plus populaire, des fabricants et distributeurs de matériel informatique vendent désormais des machines avec un système d'exploitation *Linux* pré-installé.

Parmi ceux-là, nous pouvons citer :

- [Dell](https://www.dell.com/).
- [Star Labs](https://starlabs.systems/).
- [System76](https://www.system76.com/) fournissant le fameux [Pop!_OS](https://fr.wikipedia.org/wiki/Pop!_OS), un autre dérivé d'Ubuntu.
- [Purism](https://puri.sm/), fournissant des machins sous [PureOS](https://pureos.net/), distribution orientée vers la protection de la vie privée.
- [HP](https://hpdevone.com/) fournissant des ordinateurs portables avec [Pop!_OS](https://fr.wikipedia.org/wiki/Pop!_OS) pré-installé.

Bien entendu, les distributions proposées sont également téléchargeables à part et installables sur n'importe quelle machine.
