# Installation


## Bienvenue
Lors du lancement de l'installation, une fenêtre de bienvenue s'affiche.

Choisir le *Français* à gauche pour afficher selon la langue sélectionnée.

![Installation - Bienvenue](Ubuntu_Installation_1.png "Installation - Bienvenue")

Cliquer sur **Installer Ubuntu**.

## Disposition du clavier

Vous allez pouvoir choisir la disposition de votre clavier dans la liste.

![Installation - Disposition du clavier](Ubuntu_Installation_2.png "Installation - Disposition du clavier")

Pour vérifier que vous avez choisi le bon, testez-le dans la zone prévue à cet effet.
Vous pouvez aussi utiliser l'option **Détecter la disposition du clavier**.

Cliquez sur **Continuer**.

## Mises à jour et autres logiciels

Vous allez pouvoir choisir les applications que vous désirez.

![Installation - Mises à jour et autres logiciels](Ubuntu_Installation_3.png "Installation - Mises à jour et autres logiciels")

L'**Installation normale** installera tous les logiciels standard de la distribution, tandis que l'**Installation minimale** n'installera que le minimum pour faire tourner la distribution.

Sauf si vous savez exactement ce que vous faites, gardez l'option **Installation normale** cochée par défaut.

Vous pouvez aussi choisir d'installer les mises à jour et les pilotes additionnels (vous pourrez toujours de faire plus tard).

Cliquez sur **Continuer**.

## Type d'installation

Vous allez devoir choisir un type d'installation.

Dans le cas où le disque est inutilisé, l'installation vous propose d'utiliser tout le disque.

![Installation - Type d'installation](Ubuntu_Installation_4.png "Installation - Type d'installation")

Si un autre système d'exploitation est déjà en place, l'installateur vous propose également de l'installer à côté du premier et vous devrez choisir au démarrage lequel des deux vous désirez lancer (si vous choisissez cette option).

Vous pouvez aussi choisir des **Fonctions avancées** :

![Installation - Type d'installation](Ubuntu_Installation_5.png "Installation - Type d'installation")

Elles vous permettront d'utiliser la gestion de volumes logiques (**LVM** , *logical volume management*), d'utiliser le système de fichier **ZFS** et de chiffrer le disque.

**Autre chose** vous permettra de partitionner vous-même le disque et de choisir un autre système de fichiers (BtrFS, XFS, etc.)

Sauf si vous savez exactement ce que vous faites, gardez l'option **Effacer le disque et installer Ubuntu**.

Cliquez sur **Installer maintenant**.

L'installateur affiche le résumé des opérations et vous demande de valider pour les appliquer.

![Installation - Type d'installation](Ubuntu_Installation_6.png "Installation - Type d'installation")

Vérifiez et cliquez sur **Continuer**.


## Où êtes-vous ?

L'installateur vous demande de définir votre position géographique afin de régler votre fuseau horaire.

![Installation - Où êtes-vous ?](Ubuntu_Installation_7.png "Où êtes-vous ?")

Vérifiez et cliquez sur **Continuer**.

## Qui êtes-vous ?

L'installateur vous demande qui vous êtes afin de créer votre compte.

![Installation - Qui êtes-vous ?](Ubuntu_Installation_8.png "Qui êtes-vous ?")

Ainsi il vous demande :

- **Votre nom** : Le nom qui sera affiché dans l'interface, aussi bien sur l'écran de connexion que sur votre session.
- **Le nom de votre ordinateur** : il en propose un par défaut, vous pouvez le conserver ou en utiliser un autre, pour peu qu'il n'existe pas déjà pour une autre machine.
- **Choisir un nom d'utilisateur** : il s'agit du nom technique sous lequel le répertoire *HOME* sera créé et qui contiendra tous vos fichiers. Ce nom ne doit pas contenir d'espace. Préférez les miniscules. La convention consiste généralement à prendre la première lettre du prénom et le nom, mais vous pouvez aussi utiliser un pseudonyme, un diminutif, …
- **Choisir un mot de passe** / **Confirmez votre mot de passe** : définissez un mot de passe qui vous permettra d'accéder à votre session.
- **Ouvrir la session automatiquement** : sauf cas très particulier, n'utilisez pas cette option car n'imporete qui pourra accéder à votre session sans aucune sécurité.
- **Demander mon mot de passe pour ouvrir une session** : choisissez cette option.
- **Utiliser Active Directory** : cette option est à cocher si vous utilisez *Active Directory*.

Cliquez sur **Continuer**.

Et l'installation démarre.

![Installation - Copie des fichiers](Ubuntu_Installation_9.png "Copie des fichiers")

Et l'installation démarre par la copie des fichiers, puis l'installation en elle-même.

![Installation - Installation](Ubuntu_Installation_10.png "Installation")

Vous pouvez afficher le détail en cliquant sur le chevron en bas à gauche:

![Installation - Détails](Ubuntu_Installation_11.png "Détails")

Lorsque l'installation est achevée, une fenêtre **Installation terminée** s'affiche :

![Installation - Installation terminée](Ubuntu_Installation_12.png "Installation terminée")

Cliquez sur **Redémarrer**.

L'installation vous demande de retirer le disque qui est dans le lecteur ou la clef USB qui est inséré dans le port USB.

![Installation - Redémarrer](Ubuntu_Installation_13.png "Redémarrer")

Retirez-le et appuyez sur [Entrée]. Le système redémarre.

L'écran de connexion apparaît après quelques secondes et affiche votre nom d'utilisateur (celui que vous venez de définir) :

![Installation - Connexion](Ubuntu_Installation_14.png "Connexion")

À la première connexion, l'installateur vous demande quelques informations comme les comptes auxquels vous pouvez vous connecter (vous pourrez le définir également par la suite).

![Installation - Ajustements](Ubuntu_Installation_15.png "Ajustements")

Mais aussi l'envoi des statistiques d'utilisation, la géo-localisation…
Activez et désactivez ce que bon vous semble. Vous pouvez tout refuser si vous ne savez pas quoi choisir.

Votre bureau apparaît enfin.

![Installation - Bureau](Ubuntu_Installation_16.png "Bureau")

L'installation est achevée.
Vous êtes fin prêt pour exploiter votre ordinateur.