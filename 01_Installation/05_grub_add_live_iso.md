# Avancé : ajouter le chargement d'une session live du système

En plus de charger d'anciens noyaux depuis le **Grub**, il est aussi possible d'ajouter le chargement d'une session *live*.

Il s'agit de charger l'image ISO du DVD **Ubuntu** directement au moment du démarrage et accéder ainsi à des outils pour effectuer des diagnostiques ou des actions de sauvegarde/restauration si le système de base ne fonctionne pas correctement.

## Télécharger l'ISO de la distribution

### UEFI : l'OS doit être signé

Pour des raisons de sécurité, les systèmes d'exploitation sont signés. Le gestionnaire de boot (UEFI) vérifie les signatures au démarrage.

Une signature non reconnue empêchera le chargement.
Pour cela, on ne peut qu'installer un système déjà présent sur la machine, dans la partition EFI du disque.

Si la signature est mauvaise, le chargement s'interrompt avec l'erreur :

    Mauvaise signature shim.


## Installer l'image ISO

1. Télécharger l'ISO Ubuntu depuis le site officiel : https://ubuntu.com/desktop.

2. Copier l'image ISO dans un répertoire, par exemple sur le disque racine, dans un répertoire spécial.

Pour cela, lancer **Nautilus** avec les droits administrateur depuis la ligne de commande :

    sudo nautilus

Se rendre à la racine du disque système et créer le répertoire *live*. Vérifier les droits d'écriture.

3. Copier l'image ISO dans le répertoire *live*.

## Ajouter des options au Grub

Une fois que l'image ISO est disponible, il faut indiquer au Grub de l'ajouter dans son menu de démarrage. Pour Cela, éditer le fichier /etc/grub.d/040_Custom, avec les droits d'administrateur, et ajouter les lignes suivantes :

    menuentry "Ubuntu 24.04 Live"{
    set isofile="/live/ubuntu-24.04-desktop-amd64.iso"
    loopback loop (hd0,3)/${isofile}
    linux (loop)/casper/vmlinuz boot=casper iso-scan/filename=${isofile} quiet splash
    initrd (loop)/casper/initrd
    }

Remplacer *ubuntu-24.04-desktop-amd64.iso* par le nom de votre image ISO.
Changer le numéro de disque/partition – ici *(hd0,3)* – en fonction de l'endroit où vous avez installé le fichier.

Pour déterminer le numéro de disque/partition, il suffit de lancer l'application **Disques** et de sélectionner la partition où l'image a été copiée.

Par exemple, si la partition est /dev/nvme0n1p1, et notez le premier et dernier chiffre. Cela signifie que
- nvme0n1p1 --> nvme0 --> 0 = disque **hd0**.
- nvme0n1p1 --> p1 --> 1 = partion **1**.

La notation sera alors :

    loopback loop (hd0,1)${isofile}

Enregistrer les modifications puis rafraîchir le Grub avec la commande :

    sudo update-grub

Redémarrer la machine et vérifier que l'image ISO se charge bien au démarrage lorsqu'elle est sélectionnée dans le menu **Grub**.

### Déterminer manuellement le disque/partition depuis Grub

Dans certains cas, notamment le cas de partition **Btrfs**, le chemin indiqué **set isofile="…"** peut donner une erreur de fichier introuvable. Il faut investiguer alors manuellement pour trouver le bon chemin.

Au redémarrage de la machine, accéder aux options avancées Ubuntu. **Grub** propose alors de charger différents noyaux.

Appuyer sur [c] (pour **C**ommands) afin d'accéder aux commandes en ligne de **Grub**.

**Attention** : l'entrée dans le **Grub** n'utilise pas la configuration de votre clavier mais la disposition *qwerty* (clavier américain) par défaut. Les touches ne sont pas placées au même endroit. Ayez donc une description de ce clavier sous les yeux avant d'effectuer vos opérations.

Puis lister les disques/partitions avec :

    grub> ls
    (proc) (memdisk) (hd0) (hd0,gpt1) (hd0,gpt2) …

Vous disposez ici de toutes les partitions disponibles notées sour la forme *(disque,partition)*.

Chercher le disque qui contient le répertoire *live* précédemment installé :

    grub> ls (hd0,gpt1)/

jusqu'à trouver celui qui correspond.

**Remarque** : dans le cas du **Btrfs** avec des *snapshots*, Grub peut lister un répertoire **@**. Dans ce cas, explorer ce répertoire avec :

    grub> ls (hd0,gpt3)/@/

Et utiliser le chemin **(hd0,gpt3)/@** comme racine à **live** dans le fichier */etc/grub.d/040_Custom* :

    menuentry "Ubuntu 24.04 Live"{
    set isofile="/live/ubuntu-24.04-desktop-amd64.iso"
    loopback loop (hd0,3)/@${isofile}
    linux (loop)/casper/vmlinuz boot=casper iso-scan/filename=${isofile} quiet splash
    initrd (loop)/casper/initrd
    }

**Remarque** : *(hd0,3)* est une notation raccourcie pour *(hd0,gpt3)*.

Pour quitter **Grub** et redémarrer, entrer la commande :

    exit



