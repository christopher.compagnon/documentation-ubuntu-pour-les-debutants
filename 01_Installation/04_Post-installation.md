# Après une installation Ubuntu

Après une installation fraîche *Ubuntu*, il convient de réaliser les opérations suivantes.

## Préparer un sac de survie

En cas de problème, il convient de disposer des informations et des outils nécessaires à la résolution de problèmes graves.

### Informations importantes à noter

- le *login* ayant des droits d'administateur;
- la touche [Fx] à utiliser au démarrage pour modifier l'ordre du boot afin de démarrer sur une clef USB si nécessaire;
- activer l'affichage du menu *Grub* au démarrage.


### Outils à préparer et garder précieusement

- une clef USB de capacité suffisante (8 Go) avec une distribution **Ubuntu** bootable installée dessus.

ou bien :

- une verson ISO live démarrable depuis le menu de démarrage *Grub* (voir section [Ajouter le chargement d'une session live du système](./05_grub_add_live_iso.md)).


## Configurer la sauvegarde utilisateur


## Configurer la sauvegarde système


## Installer Gnome Tweaks

**Ubuntu** utilise **Gnome** comme interface graphique qui est conçue peur êêtre simple, efficace, mais minimaliste. Les configurations sont réduites au minimum, ce qui est suffisant pour la majeure partie des usages.
Si l'on veut modifier quelques comportements, **GNOME Tweaks** est l'outil qu'il vous faut.

**GNOME Tweaks** donne accès à un certains nombre de paramétrages complémentaires et des options cachées.
Pour l'anstaller, il suffit de se rendre dans la logithèque et de rechercher *tweaks* pour afficher *Gnome Tweaks* dans les résultats.


    Check and install any Ubuntu package updates
    Set up Ubuntu Livepatch
    Install the Ubuntu Snap Store
    If you don’t like Firefox, install Chrome on Ubuntu
    Install graphics drivers if you use an Nvidia GPU
    Install VLC on Ubuntu

    Configure cloud account syncing
    Install Microsoft Core Fonts on Ubuntu
