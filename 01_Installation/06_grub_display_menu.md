# Avancé : afficher le menu Grub au démarrage du système

Si *Ubuntu* est stable et nécessite peu de maintenance, il peut lui arriver d'avoir des problèmes lorsqu'une mise à jour ne se termine pas correctement – en cas de coupure de courant ou d'extinction inopinée de la machine, par exemple.

Fort heureusement, *Ubuntu* conserve des sauvegardes des anciennes installations qui demeurent accessibles depuis *Grub* au travers d'un menu. Cependant, pour des raisons purement esthétiques, ce dernier n'est pas affiché par défaut.

**Important** : Il est fortement conseillé de l'afficher afin de pouvoir redémarrer aisément sur un ancien noyau, notamment lorsque l'utilisateur de la machine n'a pas une connaissance avancée du système. C'est toujours lorsqu'une machine qui fonctionnait bien refuse brutalement de démarrer que la panique s'installe et que les informations les plus élémentaires sont inaccessibles. Il convient donc de prendre ses dispositions en amont, au cas où, afin d'intervenir rapidement et en toute sérénité.

## Forcer l'affichage du menu Grub

Lancer l'éditeur de texte avec des droits administrateur :

    sudo gnome-text-editor

Puis éditer le fichier */etc/default/grub*. Vous pouvez aussi éditer directement ce fichier en ligne de commande avec :

    sudo vi /etc/default/grub

Modifier les lignes :

    GRUB_TIMEOUT_STYLE=hidden
    GRUB_TIMEOUT=0

en :

    GRUB_TIMEOUT_STYLE=menu
    GRUB_TIMEOUT=5

Enregistrer les modifications puis rafraîchir le *Grub* avec la commande :

    sudo update-grub

Redémarrer la machine pour vérifier que le menu s'affiche. Il doit ressembler à :

![Boot Ubuntu - affichage de menu](../06_Solutions_erreurs/Grub-Advanced-options-for-Ubuntu.webp "Boot Ubuntu - affichage de menu")

Les noyaux seront disponibles dans la section *Advanced options for Ubuntu* – ou *Options avancées pour Ubuntu* dans la version française.

