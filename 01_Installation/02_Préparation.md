# Préparation de l'installation

## Récupération de l'image ISO

Comme tout système d'exploitation à télécharger, la distribution est disponible sous la fonme d'une image ISO, c'est-à-dire un format prêt à graver sur un CD-ROM.
Si ce format peut être gravé sur un CD-ROM, il peut être aussi installé sur une clef USB.

L'image ISO est donc tout naturellement disponible sur [le site officiel de Canonical](https://ubuntu.com/download/desktop). Il suffit de choisir l'architecture  correspondant à votre machine (généralement *64bits*) et le téléchargement démarrera aussitôt que vous la sélectionnerez.

## Écriture sur une clef USB




