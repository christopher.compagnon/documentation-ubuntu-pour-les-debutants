# Ubuntu pour les simples mortels

Documentation *Ubuntu* en français pour les débutants et les simples mortels.

## À propos

[À qui s'adresse cette documentation ?](./Pour_qui.md)

## Installation

[Le choix Ubuntu](./01_Installation/01_Ubuntu.md)

[Installation](./01_Installation/03_Installation.md)

[Avancé : ajouter le chargement d'une session live du système](./01_Installation/05_grub_add_live_iso.md)

[Avancé : afficher le menu Grub au démarrage du système](./01_Installation/06_grub_display_menu.md)

## Interface

[Description de l'interface utilisateur](./02_Interface/01_Présentation.md)

[Les logiciels pré-installés](./02_Interface/02_Liste_applications.md)

[Programmes, applications, fenêtres](./02_Interface/03_Applications.md)

[Personnalisation de l'interface](./02_Interface/Personnalisation.md)

[Gérer l'affichage des écrans haute définition](./02_Interface/Manage_display.md)

## Maintenance

[Mises à jour](./03_Maintenance/01_Updates.md)

[Mises à niveau](./03_Maintenance/02_Upgrade.md)

[Réinstallation du système](./03_Maintenance/03_Reinstall.md)

## Paramètres et configuration

[Les paramètres](./02_Interface/02_Paramètres.md)

[Dconf](./03_Parametres/02_dconf.md)

[Extensions](./03_Parametres/03_Extensions.md)

## Périphériques

[Claviers et raccourcis](./02_Interface/04_Raccourcis.md)

[Gestion des imprimantes et impression](./04_Peripheriques/01_Imprimantes.md)

[Les gestes au pavé tactile (*touchpad*)](./04_Peripheriques/Touchpad_Gestures.md)

## Impression

[Installation et configuration d'imprimante](./05_Impression/Installation.md)

[Résolution des problèmes d'impression](./05_Impression/Résolution.md)

## Utiliser son ordinateur

[Connecter du matériel](./04_Usages/01_External_devices.md)

[Formater un disque](./04_Usages/02_Formater_disque.md)

[Numériser un document](./04_Usages/03_Scanner.md)

[Gestion des répertoires et fichiers](./04_Usages/04_Files_management.md)

## Logiciels

[Les logiciels pré-installés](./02_Interface/02_Liste_applications.md)

[Installation/désinstallation de logiciels](./Logiciels/Installation.md)

## Configurations avancées


[Basculer sur l'affichage console](./06_Solutions_erreurs/00_connect_to_console.md)

[Redémarrer sur un ancien noyau](./06_Solutions_erreurs/01_reboot_older_kernel.md)

[Modifier l'affichage au démarrage](./06_Solutions_erreurs/01_modify_grub.md)

[Problème d'affichage de session](./06_Solutions_erreurs/01_Session_display.md)

[Reconfigurer le clavier par défaut (système)](./06_Solutions_erreurs/04_reconfigure_system_keyboard.md)



