# À propos de Ubuntu

Linux est un noyau.

Pour le commun des mortels, un noyau ne sait que gérer le matériel de l'ordinateur, c'est-à-dire accéder au disque dur, à la mémoire vive, aux interfaces (clavier, souris, etc.) et c'est à peu près tout.

Pour que la machine soit utilisable, il faut des programmes.

Linux étant pensé au départ comme un noyau libre, il lui fallait également des programmes libres. Or de tels programmes existaient déjà  et étaient rassemblés sous l'appelation **GNU** parce qu'ils étaient prévus pour fonctionner avec le [système d'exploitation GNU](https://fr.wikipedia.org/wiki/GNU).

Cet ensemble de programmes et ce noyau sont appelés Linux, mais le terme plus exact est GNU/Linux ou, pour les puristes, GNU+Linux, afin de bien indiquer qu'il s'agit de programmes associés au noyau Linux (et non pas celui prévu au départ pour GNU).

Pour être fidèles à la philosophie initiale du projet GNU, les [logiciels sont libres](https://fr.wikipedia.org/wiki/Logiciel_libre).


## Les distributions
Le noyau et les programmes mis à disposition étant libres, les utilisateurs ont rapidement créés leurs propres systèmes d'exploitation en fonction de leurs goûts et de leurs usages/besoins. Certaines distributions incluent une plhilosophie ou un contrat social, comme c'est le cas pour **Debian**.

Ces systèmes d'exploitation empaquetés et proposés comme un tout sont nommés *distributions*.

Ils comprennent généralement :
- le noyau Linux;
- une interface graphique;
- des logiciels.

**Ubuntu** est une des distributions Linux disponibles aujourd'hui mais il en existe beaucoup d'autres.


## Les saveurs

Une même distribution peut être proposée avec une interface graphique différente de celle prévue au départ, le reste étant à peu près identique.
Cette variante est appelée *saveur*.

Pour Ubuntu, il existe différentes saveurs :
- **[Ubuntu](https://ubuntu.com/)** : la saveur naturelle utilisant l'interface graphique **Gnome**;
- **[Kubuntu](https://kubuntu.org/)** : utilisant l'interface graphique **KDE**;
- **[Ubuntu Mate](https://ubuntu-mate.org/)** : utilisant l'interface graphique **Mate**;
- **[Xubuntu](https://xubuntu.org/)** : utilisant l'interface graphique **Xfce**;
- **[Lubuntu](https://lubuntu.net/)** : utilisant l'interface graphique **Lxde**;
- **[Ubuntu DDE](https://ubuntudde.com/)** : utilisant l'interface graphique **Deepin Desktop Environment**, l'environnement de bureau développé pour **[Deepin Linux](https://www.deepin.org/en/)**, distribution fournie par Wuhan Deepin Technology Co.;
- **[Ubuntu Budgie](https://ubuntubudgie.org/)** : utilisant l'interface graphique **Budgie**, l'environnement de bureau développé pour **[Solus OS](https://getsol.us/home/)**, une autre distribution Linux;
- **[Ubuntu Kylin](https://www.ubuntukylin.com/index-en.html)** : variante Ubuntu spécialement destinée à la Chine.

Il existe aussi :
- **[Ubuntu Studio](https://ubuntustudio.org/)** : une distribution spécialement conçue pour la production audio, vidéo, sonore et graphique. Cette distribution utilise l'interface graphique **KDE** ou **Xfce**, un noyau optimalisé (latente faible) et un ensemble de logiciels adaptés.


## Les autres distributions fondées sur Ubuntu

Chacun étant libre de prendre le code source, de le modifier pour le redistribuer, il existe des distributions créées à partir de Ubuntu.
Parmi celles-ci, nous pouvons citer :

- **Linux Mint** : 
- **Zorin OS** : 
- **Elementary OS** :
- **Trisquel** : distribution épurée des paquets non-libres.

## Les spécificités d'Ubuntu
