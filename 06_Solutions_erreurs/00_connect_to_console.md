# Basculer sur l'affichage console

L'affichage par défaut est une session graphique. Mais il existe d'autres sessions permettant de contourner cet affichage et accéder à l'ordinateur depuis un terminal.

Cette connexion alternative est accessible depuis la combinaison de touches [Ctrl] + [Alt] + [F3].

Remarques :

1. en fonction des claviers – notamment sur les ordinateurs portables –, les touches *Fx* sont parfois accessibles au travers de la touche « fonction » [Fn]. Dans ce cas, il faudra taper [Ctrl] + [Fn] + [Alt] + [F3].
2. la touche [F3] n'est pas systématique, elle peut dépendre de votre système. C'est parfois [F1], [F2], [F4]. Si vous n'obtenez pas votre session terminal en utilisant [F3], alors essayez une autre touche Fx. Notez seulement que la touche [F2] est *normalement* la session graphique.

Lorsque vous avez basculé en console, un écran noir occupe tout l'écran et vous demande votre *login*. Entrez donc le nom de l'utilisater (login) puis son mot de passe.

## Remarques

### Clavier par défaut en américain (QWERTY)

Lors de la bascule en console, il y a de fortes chances que le clavier soit en disposition américaine (QWERTY), ce qui risque de compliquer la situation, notamment si vous devez taper des caractères qui ne seront pas affichés – le mot de passe.

Avant de faire quoi que ce soit, tapez votre mot de passe en clair à la place de l'utilisateur (Login) afin de bien mémoriser les touches. Le plus simple est de noter la correspondance sur un morceau de papier et de le garder à côté de vous le temps de la procédure.

Une fois que vous avez le mot de passe, vous l'effacez et vous remplacez par l'utilisateur (Login) puis vous entrez ensuite de mot de passe à l'invitation de la console.

### Affichage du mot de passe

Pour des raisons de sécurité, le mot de passe ne s'affiche pas. Il n'y aura pas non plus d'astérisques qui pourraient donner une indication sur la longueur de mot de passe si une personne mal intentionnée regardait votre écran par-dessus votre épaule. Il faudra donc taper le mot de passe à l'aveugle.

Il se peut que le clavier par défaut du système ne soit pas configuré comme celui de la session. Cette disposition peut entraîner des problèmes lors de l'entrée du mot de passe. Pour éviter cela, il convient de :

1. taper le mot de passe à la place du *login* (utilisateur) demandé (**sans appuyer** sur [Entrée]) pour valider la disposition des touches et les avoir en mémoire (les noter si besoin);
2. effacer le mot de passe du *login* puis entrer le nom d'utilisateur (login) et valider par [Entrée]);
3. à l'invitation du mot de passe (*password*), entrer le mot de passe précédemment déterminé.

