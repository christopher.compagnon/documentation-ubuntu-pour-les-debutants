# Modifier l'affichage au démarrage

## Pré-requis

Vous êtes dans un des cas suivants :
1. vous avez accès à la session de votre ordinateur sans aucun problème et vous pouvez lancer un terminal;
2. vous pouvez accéder localement à votre ordinateur à partir de la console.

## Afficher les options avancées de démarrage

Vous êtes connecté à votre ordinateur depuis votre session – et vous avez lancé le terminal – ou la console.

### Créer une sauvegarde du fichier /etc/default/grub

Rendez-vous dans le répertoire de configuration par defaut du système :

    cd /etc/default

**Explication** : *cd* (change directory) permet de changer de répertoire et se rendre à l'endroit indiqué */etc/default*.

Effectuez une copie de fichier de configuration **grub** :

    sudo cp grub grub.bkp

La commande *sudo* demande le mot de passe de l'administrateur. Entrez-le pour exécuter la commande.

**Explication** : *cp* (copy) copie le fichier *grub* en une version de sauvegarde (bkp = backup) afin de pouvoir le rescaurer le cas échénat. Il est fortement conseillé de créer une copie de sauvegarde avant toute modification d'un fichier système.

### Modifier la configuration

Maintenant que vous avez votre fichier en sécurité, vous pouvez le modifier. Pour cela, vous allez utiliser le programme **vi**. Il est un peu exotique, mais ses combinaisons de touches sont plus faciles à identifier, notamment si la configuration du clavier ne correspond pas à celui en place.

Avant toute chose, veuillez identifier sur votre clavier et confirmer la position des touches suivantes : i, x, w, q, !, :

Maintenant que vous avez bien noté leur position, tapez la commande suivante :

    sudo vi grub

**Explication** : *vi* édite le fichier *grub*. Vous accédez donc à son contenu. Il est pour l'instant en lecture seule. Aucune modification n'est permise tant que vous n'en donnez pas l'ordre.

À l'aide des flèches haut/bas de votre clavier, rendez-vous sur la ligne :

    GRUB_TIMEOUT_STYLE=hidden

À l'aide des flèches droite/gauche de votre clavier, positionnez-vous sur le *n* de *hidden* – donc la dernière lettre du mot.

Appuyez sur la touche *x* de votre clavier. Cette opération efface la lettre où vous vous trouvez. Répétez l'opération pour effacer le mot *hidden* lettre par lettre. Vous obtenez donc :

    GRUB_TIMEOUT_STYLE=

À l'aide des flèches haut/bas de votre clavier, rendez-vous sur la ligne :

    GRUB_TIMEOUT=0

À l'aide des flèches droite/gauche de votre clavier, positionnez-vous sur le *0* et effacez-le en appuyant sur la touche *x* de votre clavier.

Appuyez sur la touche *i* (*i* = insertion) de votre clavier puis tapez *2*; vous obtenez :

    GRUB_TIMEOUT=2

Appuyez sur [Esc] pour sortir de mode *insertion*.

**Astuce** : si vous vous trompez, pas de panique. Tant que vous ne tapez pas la commande «:w», rien n'a été modifié réellement. Vous pouvez tout annuler en tapant simplement (sans les guillemets) [Esc] puis «**:q!**» pour forcer à quitter. Recommencez ensuite les opérations d'édition depuis **Modifier la configuration**.

Dans le pire des cas, même si vous avez validé les modifications, elles ne seront pas appliquées tant que vous n'aurez pas demandé à *grub* d'en tenir compte par la commande *update-grub*. Donc pas de panique.

### Sauver la configuration

Sauvez les modifications de fichier en tapant successivement (sans les guillemets) «**:w**».

**Explication** : **:w** (write) permet écrire les modifications.

Fermez le fichier en tapant successivement (sans les guillemets) «**:q**».

**Explication** : **:q** (quit) pour quitter, donc fermer le fichier.

Vous revenez alors à la console/terminal.

Pour appliquer la nouvelle configuration, tapez la commande :

    sudo update-grub

Vous pouvez redémarrer avec :

    sudo reboot

Durant l'opération de démarrage, l'écran suivant s'affichera durant 2 secondes :

![Boot Ubuntu - affichage de menu](Grub-Advanced-options-for-Ubuntu.webp "Boot Ubuntu - affichage de menu")


