# Réparer le GRUB

La restauration d'un démarrage sous Linux à partir du mode GRUB implique généralement de corriger la configuration GRUB ou de réinstaller GRUB. Voici quelques étapes générales que vous pouvez suivre pour restaurer le démarrage sous Linux à partir du mode GRUB :

 ## Démarrer sur un Live CD/USB
 
 Si vous ne parvenez pas à démarrer votre système Linux, vous devrez démarrer à partir d'un live CD ou d'une clé USB. La plupart des distributions Linux proposent une option « Essayer Linux sans installer » qui vous donnera accès à un environnement de bureau.

##  Identifier la partition Linux

 Une fois que vous êtes dans l'environnement réel, vous devez identifier la partition sur laquelle votre système Linux est installé. Vous pouvez utiliser des outils comme **lsblk** ou **fdisk -l** pour répertorier les partitions disponibles et trouver celle qui contient votre installation Linux.

 ## Monter la partition Linux
 
 Vous devez monter la partition Linux sur laquelle votre système est installé. Vous pouvez le faire en créant un point de montage (par exemple, */mnt*), puis en montant la partition dans ce répertoire. Par exemple, si votre partition Linux est */dev/sda1*, vous pouvez la monter à l'aide de la commande suivante :

    sudo mount /dev/sda1 /mnt

 ## Chroot dans le système Linux 
 
 Une fois la partition montée, vous devez chrooter dans votre système Linux. Cela vous permettra d'exécuter des commandes comme si vous étiez démarré sur votre système actuel. Vous pouvez le faire avec la commande suivante :

     sudo chroot /mnt

 ## Réinstaller GRUB
 
 Pour réinstaller GRUB, vous pouvez utiliser la commande **grub-install**. La commande exacte peut varier en fonction de votre distribution. Par exemple, pour les systèmes utilisant *GRUB 2*, vous pouvez utiliser la commande suivante :

    grub-install /dev/sda

 ## Mettre à jour la configuration GRUB
 
 Après avoir réinstallé GRUB, vous devrez peut-être mettre à jour le fichier de configuration GRUB. Vous pouvez le faire en exécutant la commande **update-grub** :

    update-grub

 ## Quitter Chroot et redémarrer
 
 Une fois que vous avez réinstallé GRUB et mis à jour la configuration, vous pouvez quitter l'environnement *chroot* et redémarrer votre système :

    exit
    reboot

Ces étapes devraient vous aider à restaurer le démarrage sous Linux à partir du mode GRUB. Gardez à l'esprit que les commandes et étapes exactes peuvent varier en fonction de votre distribution et de votre version Linux. Soyez prudent lorsque vous apportez des modifications à la configuration de démarrage de votre système afin d'éviter toute perte potentielle de données ou instabilité du système.