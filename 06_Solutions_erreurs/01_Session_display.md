# Problème d'affichage de session


## Description du problème

Lorsque vous vous connectez ou que vous lancez une application, um message apparaît sur l'écran :

    Oh mince ! Quelque chose c'est mal passé.
    Un problème est survenu et le système ne peut pas se récupérer.
    Déconncetz-vous et eayyez à nouveau.

Avec un bouton  « Fermer la session » placé au dessous.

## Causes probables

La première cause est très probablement une mise à jour qui a été interrompue et ne s'est pas achevée correctement. Certains programmes et/ou configurations sont invalides et provoquent l'instabilité du système.


## Solutions

### Achever une mise à jour interrompue

#### Basculer sur l'affichage console

L'affichage par défaut est une session graphique. Mais il existe d'autres sessions permettant de contourner cet affichage et accéder à l'ordinateur depuis un terminal.

Cette connexion alternative est accessible depuis la combinaison de touches : [Ctrl] + [Alt] + [F3]

Remarques :

1. en fonction des claviers – notamment sur les ordinateurs portables –, les touches *Fx* sont parfois accessibles au travers de la touche « fonction » [Fn]. Dans ce cas, il faudra taper : [Ctrl] + [Fn] + [Alt] + [F3].
2. la touche [F3] n'est pas systématique, elle peut dépendre de votre système. C'est parfois [F1], [F2], [F4]. Si vous n'obtenez pas votre session terminal en utilisant [F3], alors essayez une autre touche Fx. Notez seulement que la touche [F2] est *normalement* la session graphique.

Lorsque vous avez basculé en console, un écran noir occupe tout l'écran et vous demande votre *login*.

#### Se connecter à l'ordinateur

Une fois que vous avez la console à l'écran, utilisez votre utilisateur (*login*) et mot de passe habituels pour vous connecter.

#### Achever la mise à jour

Vous étes connecté à votre ordinateur en mode console.

Pour achever les mises à jour, tapez les commandes suivantes :

    sudo apt update

Exécuter en appuyant sur [Entrée].

- **sudo** : permet d'exécuter des opérations en mode administrateur – *su* = super utilisateur.
- **apt** : est le gestionnaire de paquet qui permet de gérer les logiciels et les mises à jours.
- **update** : permet d'effectuer la mise à jour de la base de données des paquets logiciels.

Remarque : La commande **sudo** vous demande d'entrer le mot de passe utilisateur. Entrez votre mot de passe.

Une fois que cette opération est achevée, appliquer les mises à jour par :

    sudo apt dist-upgrade

Exécuter en appuyant sur [Entrée].

Si cette opération finit en erreur, pas de panique. Il suffit de **Reconfigurer le gestionnaire de paquet**.

Si cette opération indique que des paquets sont brisés (broken) lancer la procéduse de **Réparation de paquet**.

#### Reconfigurer le gestionnaire de paquet.

Pour le gestionnaire de paquet, entrer la commande :

    sudo dpkg --configure -a

Exécuter en appuyant sur [Entrée].

Cette commande va reconfigurer la procédure de mise à jour du système. Si des questions sont posées, choisir l'option *N* (option par défaut)  pour utiliser la version actuellement présente.

Une fois cette opération achevée, recommencer la procédure **Achever la mise à jour**.

#### Réparation de paquet

En cas de paquets brisés, entrer la commande :

    sudo apt --fix-broken install

Exécuter en appuyant sur [Entrée].
Le système va réinstaller le paquets qui ont perdu leurs dépendances.

Une fois cette opération achevée, recommencer la procédure **Achever la mise à jour**.


#### Nettoyer l'installation

Pour nettoyer l'installation de tous les déchets, entrer la commnade :

    sudo apt clean

Exécuter en appuyant sur [Entrée].

Puis :

    sudo apt autoremove

Exécuter en appuyant sur [Entrée].

#### Relancer la machine

Pour redémarrer la machine, entrer la commande :

    sudo reboot now

Exécuter en appuyant sur [Entrée].

#### Se connecter en mode graphique (mode normal)

Une fois redémarrée, la machine propose de mode graphique par défaut. Vérifier que le problème est résolu.
