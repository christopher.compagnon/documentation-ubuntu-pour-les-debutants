# Reconfigurer le clavier par défaut (système)


## Problématique

Dans certains cas, le clavier du système par défaut n'est pas le même que celui des sessions. En effet, tout dépend de la disposition du clavier utilisée lors de l'installation par la personne qui a installé le système.

Cette diposition pose problème dans les cas suivants :

- au moment de la connexion à la session : l'utilisateur doit taper son mot de passe selon la disposition par défaut et non celle confiqurée dans sa session;
- lors d'opérations de maintenance en mode console : le clavier utilisé est alors celui du système, ce qui complique l'exécution de commande avancées.

## Solution

Depuis le terminal, taper la commande :

    sudo dpkg-reconfigure keyboard-configuration

Et suivre les indications pour sélectionner le bon clavier.
