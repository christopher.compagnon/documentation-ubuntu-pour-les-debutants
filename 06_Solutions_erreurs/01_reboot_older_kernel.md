# Redémarrer sur un ancien noyau

**Linux** – et donc **Ubuntu** – installe régulièrement des mises à jour dont la fréquence dépend de la distribution. Parmi ces mises à jour, de nouvelles versions du noyau pour corriger des petits problèmes, qu'ils soient de fonctionnement ou de sécurité.

Lorsqu'un nouveau noyau est installé, l'ancien n'est pas supprimé pour autant. En cas de problème, il reste disponible afin de basculer dessus pour disposer d'un système dans une version antérieure qui fonctionnait normalement. 

Dans le cas d'une installation qui n'est pas achevée, par exemple, le nouveau noyau installé peut subir des dommages et ne pas être pleinement fonctionnel. Utiliser un ancien noyau est une solution simple pour restaurer un système stable. 

Redémarrar sur un ancien noyau permet de reprendre la main sur un système défaillant et le corriger si le système n'est pas capable de le faire par lui-même.

## Procédure

### Redémarrer la machine avec les options avancées

Arrêtez et redémarrez la machine.
Au moment du redémarrage, l'écran suivant peut apparaître furtivement durant quelques courtes secondes :

![Boot Ubuntu - affichage de menu](Grub-Advanced-options-for-Ubuntu.webp "Boot Ubuntu - affichage de menu")

Si cet écran n'apparaît pas mais bascule immédiatement sur le logo **Ubuntu** puis à l'écran de connexion de votre session, vous pouvez forcer l'affichage de deux façons :

1. en modifiant la configuration de l'écran de démarrage : suivez la procédure [Modifier l'affichage au démarrage](./06_Solutions_erreurs/01_modify_grub.md);
2. en forçant l'affichage de ce menu par une opération *ad hoc*, en redémarrant la machine et en appuyant juste après sur la touche [Shift] ou [Esc]. La touche dépend du système, notamment si elle est également utilisée pour entrer dans la configuration du [bios](https://fr.wikipedia.org/wiki/BIOS_(informatique)) et éviter ainsi un conflit. Commencez cependant par tester avec la touche [Shift] qui est peu usitée en dehors de ce cas afin de l'éliminer rapidement.

**Remarque** : dans ce dernier cas, le forçage de cet affichage nécessite une bonne synchronisation entre l'appui sur la touche [Shift] ou [Esc] et la procédure de chargement. Il faut donc faire preuve de patience et répéter l'opération plusieurs fois en redémarrant la machine si nécessaire.

### Tester la touche [Shift]

Lorsque vous redémarrez la machine, laissez la touche [Shift] enfoncée jusqu'à ce que quelque chose s'affiche. Si vous accédez à l'écran de connexion aux sessions utilisateur, vous venez d'exclure la touche. Dans ce cas, testez avec [Esc].

La touche [Shift] est représentée sous la forme d'une flèche épaisse dirigée vers le haut. Dans son usage normal, elle sert à basculer (*Shift*) en majuscule. Sur la plupart des claviers, il y a deux touches [Shift] : une à droite et une à gauche. Sur la gauche, elle est normalement positionnée au-dessus de la touche [Ctrl].

Dans le cas d'*Ubuntu*, il faut utiliser la touche [Shift] de droite.

### Tester la touche [Esc]

La touche [Esc] est normalement moins fréquente, car elle est très utilisée pour entrer dans le BIOS au démarrage.

Lorsque vous redémarrez la machine, appuyez régulièrement – par intervalle de 1/2 ou 1/3 de seconde – jusqu'à ce que quelque chose s'affiche. Attendez un peu pour vérifiez que les opérations s'achèvent. Si vous n'obtenez pas l'écran de connexion, c'est plutôt bon signe. Dans ce cas, forcez le redémarrage de la machine en appuyant 10 secondes sur la touche de démarrage de votre ordinateur.

Lors du redémarrage, attendez l'affichage du premier logo – normalement celui de la marque de votre ordinateur – et appuyez une fois sur [Esc]. Si tout se passe bien, le menu des options avancées doit apparaître. Si ce n'est pas le cas, recommencez : forcez le redémarrage de la machine et appuyez sur [Esc] à un autre moment du démarrage.

### Sélection de l'option avancée

Vous disposez à présent de l'écran d'options de démarrage. Cet affichage propose :

- **Ubuntu** : lancement normal Ubuntu – sélection par défaut.
- **Advanced** options for Ubuntu : les options avancées.
- d'autres options qui ne nous intéressent pas ici.

À l'aide des flèches haut/bas de votre clavier, sélectionnez les options avancées et validez par [Entrée].

À ce moment-là, ces dernières apparaissent de la façon suivante :

![Boot Ubuntu - options avancées](Grub-options.webp "Boot Ubuntu - options avancées")

Nous avons là l'ensemble des noyaux encore disponibles. Celui tout en haut est le noyau actuel, plus nous descendons dans la liste, plus les noyaux sont anciens – on les reconnaît grâce à leur numéro de version moins élevée.

Il y a deux types d'options :

- le noyau normal;
- le noyau en mode récupération (*recovery mode*).


### Démarrer sur la précédente version du noyau



