# Description des problèmes

## Avertissement

Cette partie traite des problèmes qui n'ont pas été résolus par une méthode classique. Si vous êtes ici , c'est que vous ne pouvez malheureusement pas faire autrement car toutes les autres solutions ont échoué. En désespoir de cause, les solutions envisagées nécessitent l'usage du terminal et de la ligne de commande.

Les opérations traitées ici peuvent modifer profondément le comportement du système et provoquer plus de mal que de bien si elles sont mal exécutées. Veuillez toutefois vous assurer que vous avez vraiment appliquées les autres solutions proposées avant de poursuivre.

Si vous n'êtes pas familier de la ligne de commande, faites en sorte d'être assisté par une personne qui peut vous guider.

## Oh mince ! Quelque chose c'est mal passé

### Description

Lorsque vous vous connectez ou que vous lancez une application, um message apparaît sur l'écran :

    Oh mince ! Quelque chose c'est mal passé.
    Un problème est survenu et le système ne peut pas se récupérer.
    Déconnectez-vous et essayez à nouveau.

Avec un bouton  « Fermer la session » placé au dessous.

### Résolution

