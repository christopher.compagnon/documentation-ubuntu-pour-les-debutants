# Mise à niveau

La mise à niveau est l'installation d'une nouvelle version d'**Ubuntu**, soit :
- tous les 6 mois, si vous décidez de mettre à niveau à chaque nouvelle version;
- tous les 2 ans si vous mettez à niveau que les versions de support à long terme (**LTS** : **L**ong **T**erm **S**upport).

## Vérification de la fréquence

La fréquence se vérifie dans l'application **Logiciels et mises à jour**, onglet *Mises à jour* :

![Logiciels et mises à jour - Fréquence de mise à jour](Ubuntu_Mises_a_jour.png "Fréquence de mise à jour")

L'option «*Me prévenir lorsqu'une nouvelle version d'Ubuntu est disponible*» propose 3 valeurs :

- *Pour chaque nouvelle version* : soit 2 fois par an, une fois en avril et un fois en octobre;
- *Pour les versions prises en charge sur le long terme* : soit une fois tous les deux ans, les années paires au mois d'avril (20.04, 22.04, etc.);
- *Jamais*.

**Attention** : À chaque version long terme (*LTS*) mise à niveau, le système définit l'option «*Pour les versions prises en charge sur le long terme*» comme nouvelle valeur. Si vous aviez choisi une autre valeur, elle a donc été remplacée et il faut donc la modifier à nouveau.

## Lancement manuel


## Exécution

Lorsqu'une nouvelle version **Ubuntu** est disponible, le système l'indique par un petit message :

![Mise à niveau - Nouvelle version](Ubuntu_Upgrade_check.png "Nouvelle version disponible")

Pour lancer la mise à niveau, fermez toutes vos applications (sauf la fenêtre du Gestionnaire de mise à jour ci-dessus) et cliquez sur «*Mise à niveau*».

Un message détaille le contenu de cette version :

![Mise à niveau - Nouvelle version](Ubuntu_Upgrade_welcome.png "Nouvelle version disponible")

Cliquez sur «*Mettre à niveau*».

L'installation se lance en commençant par quelques vérifications…

![Mise à niveau - Vérification](Ubuntu_Upgrade_run1.png "Vérification")

Et il affiche un détail avec un avertissement :

![Mise à niveau - Avertissement](Ubuntu_Upgrade_warning.png "Avertissement")

Cliquez sur «*Lancer la mise à niveau*».

La procédure s'exécute étape par étape.

![Mise à niveau - Exécution](Ubuntu_Upgrade_run.png "Exécution")

Le détail peut être affiché en cliquant sur le chevron à côté de «Termninal».

![Mise à niveau - Exécution détaillée](Ubuntu_Upgrade_details.png "Exécution détaillée")

Une fois l'installation achevée, il propose de nettoyer les paquets obsolètes :

![Mise à niveau - Paquets obsolètes](Ubuntu_Upgrade_remove.png "Paquets obsolètes")

Vous pouvez choisir de les conserver ou de les supprimer.

Puis une proposition de redémarrage pour finaliser le tout :

![Mise à niveau - Reboot](Ubuntu_Upgrade_reboot.png "Reboot")

Cliquez sur «*Redémarrer maintenant*».

