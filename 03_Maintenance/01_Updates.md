# Mises à jour


## Mises à jour automatiques

**Ubuntu** exécute des mises à jour automatiques en tâche de fond. Lorsqu'elles sont prêtes, il propose de les appliquer.

Une fenêtre apparaîtra alors automatiquement :

![Gestionnaire de mises à jour - Mises à jour disponibles](Ubuntu_updates_1.png "Gestionnaire de mises à jour - Mises à jour disponibles")

Vous pouvez reporter l'installation en cliquant sur «*Me le rappeler plus tard*» ou procéder à l'installation immédiate en cliquant sur «*Installer maintenant*».

Lorsque l'installation est achevée, il y a deux cas possibles.

**Dans le premier cas**, l'installation requiert un redémarrage de votre machine lorsque les mises à jour incluent des correctifs liés à la sécurité.

**Ubuntu** vous propose alors un redémarrage pour rendre les changements effectifs et définitifs :

![Maintenance - Redémarrage](Ubuntu_updates_2.png "Redémarrage")

Vous pouvez redémarrer immédiatement en cliquant sur «*Redémarrer maintenant*» ou reporter l'opération en cliquant sur «*Redémarrer ultérieurement*».

**Dans le second cas**, les correctifs sont des mises à jour mineures. Il s'agit généralement de mises à jour logicielles pour disposer des nouvelles versions. La mise à jour s'appliquera alors au moment du redémarrage de votre application et il n'est nul besoin de redémarrer le système pour en profiter.

**Ubuntu** vous indique simplement que votre système est à jour.

## Mises à jour manuelles

Si vous avez reporté les mises à jour en fermant la fenêtre, vous pouvez les vérifier/appliquer à tout moment en lançant l'application «Gestionnaire de mises à jour» en la recherchant depuis la zone de recherche des applications :

![Maintenance - Gestionnaire de mise à jour](Lancer_mises_a_jour.png "Maintenance - Gestionnaire de mise à jour")

L'application vérifie les mises à jour disponibles :

![Gestionnaire de mises à jour - vérification des mises à jour](Gestionnaire_de_mises_a_jour_2.png "Gestionnaire de mises à jour - vérification des mises à jour")

Puis propose les mises à jour s'il y en a :

![Gestionnaire de mises à jour - Mises à jour disponibles](Ubuntu_updates_1.png "Gestionnaire de mises à jour - Mises à jour disponibles")

Il suffit de cliquer sur «*Installer maintenant*» et de suivre la procédure.


