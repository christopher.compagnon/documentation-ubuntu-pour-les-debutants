# Réinstallation du système

Il peut parfois être nécessaire de réinstaller le système si :
- à force d'usage, le système devient trop instable;
- le matériel avait été initialement mal calibré et il faut changer une partie – cas du disque système trop petit et il devient impossible de réaliser des mises à jour ou des mises à niveau;
- il faut passer sur une machine plus récente parce que l'actuelle est trop vieille ou qu'elle ne fonctionne plus très bien;
- etc.

Il existe donc deux cas distints. Une installation sur :
- la même machine – réinstallation.
- une machine différente – migration

Bonne nouvelle : dans les deux cas, la procédure est la même.

## Le répertoire /home

Toutes les données utilisateur sont stockées dans le répertoire **/home**. Le point principal de la procédure consiste à sauver ce répertoire puis le restaurer sur le système nouvellement installé.

## Sauvegarde

### Noter les mots de passe maître

Si vous disposer d'applications ayant un mot de passe maître, assurez-vous de bien disposer de ce mot de passe. Il vous sera demandé lorsque vous lancerez l'application sur le système nouvellement installé.

C'est le cas, par exemple, du gestionnaire de mots de passe de *Firefox* qui stocke le mot de passe maître dans un compte en ligne contenant son propre mot de passe est stocké. Sans lui, le compte entier est bloqué. Il faut donc entrer dans le gestionnaire de mots de passe et noter celui correspondant au compte *Firefox*.

### Sauvegarder /home

Pour la sauvegarde, copier le répertoire sur un disque externe – réseau ou USB – de capacité suffisante.
Pour connaître la capacité :

Ouvrir le gestionnaire de fichiers. Se rendre dans la section «**Autres emplacements**» – dans le volet gauche tout en bas – puis sur le disque système «**Ubuntu**».

Dans la liste des répertoires affichés, chercher «**home**» et afficher les options – clic-droit – puis «**Propriétés**».

Le système va alors afficher l'espace total utilisé par le répertoire. 

**Remarque** : la capacité totale n'est pas instantanée s'il y a beaucoup de données stockées sur le disque. Dans ce cas, il faut attendre la fin de l'opération.

Une fois le disque dur connecté à la machine et formaté correctement – voir la section [Formater un disque](../04_Usages/02_Formater_disque.md) pour en savoir plus –, fermer toutes les applications – sauf le gestionnaire de fichiers – et copier le répertoire **/home** sur le disque externe. L'opération peut prendre un peu de temps, en fonction de la vitesse de l'interface et de la quantité de données.

## Intallation



## Restauration de la sauvegarde

