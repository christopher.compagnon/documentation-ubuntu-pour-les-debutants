# Formater un disque

Par défaut, un disque n'est qu'un support de stockage physique. On peut y écrire n'importe quoi.
Pour que le contenu du disque ait un sens pour la machine, qu'elle sache comment interpréter les informations écrites, il faut lui définir un format qui lui permettra de savoir comment écrire et lire le contenu.

Formater un disque signifie appliquer ce format sur le support (ici : le disque).

Chaque format est identifié par un nom qui provient généralement d'un acronyme.

## Les différents formats

### exFAT

**exFAT** (*Extended File Allocation Table*) est un système de fichiers propriétaire conçu par **Microsoft Corporation** principalement pour les mémoires flash et les supports de stockage externes (disques durs et assimilés).

C'est le système de fichiers par défaut des cartes SD. 

Le noyau **Linux** introduit la prise en charge native du système de fichiers **exFAT** avec la version 5.4.

**À retenir** : Ce format étant disponible ou accessible sur la plupart des systèmes d'exploitation, il devient, de fait, celui utilisé pour l'interopérabilité quand il s'agit, par exemple, d'échanger des données entre différents systèmes d'exploitation.

### NTFS

**NTFS** (de l'anglais *New Technology File System*) est un système de fichiers développé par **Microsoft Corporation** pour sa famille de systèmes d'exploitation **Windows NT**, à partir de Windows NT 3.1, **Windows 2000** et utilisé depuis par tous leurs successeurs (XP, Server 2003, 7, etc.).

### EXT

**EXT** (**Ext**ended file system) est le premier système de fichiers créé en avril 1992 spécifiquement pour le système d'exploitation **Linux**.

## Disques : un logiciel versatile

Le formatage d'un disque dur s'effectue avec le logiciel **Disques**, installé par défaut sur **Ubuntu**. Ce logiciel regroupe un ensemble d'outils pour manipuler les disques comme le formatage, la sauvegarde/restauration d'un disque, le redimensionnement d'une partition, etc.


## Formater un disque dur

Pour formater un disque qu'il soit interne ou externe (USB, eSATA, etc.), la procédure est la même.

Si le disque est installé/connecté correctement, il apparaît dans la liste des disques disponibles au lancement de **Disques**, sur le panneau de gauche.

Sélectionner le disque à formater.

![Formater un disque - Sélectionner le disque](Format_disk1.png "Formater un disque - Sélectionner le disque")

Depuis les options (bouton avec trois points verticaux, en haut à droite), sélectionner **Formater le disque** :

![Formater un disque - Sélectionner le disque](Format_disk2.png "Formater un disque - Sélectionner le disque")

Une nouvelle fenêtre s'ouvre pour afficher les options de formatage :

![Formater un disque - Sélectionner le disque](Format_disk3.png "Formater un disque - Sélectionner le disque")

Les options disponibles sont :

- **Effacer** : choisissez *Ne pas pas écraser les données existantes* si vous voulez un formatage rapide ou si le disque est de petite capacité. Sinon, l'effacement complet des données peut être beaucoup plus long et inutile dans la plupart des cas.
- **Partitionnement** : choisissez *Compatible avec les systèmes modernes et les disques durs> à 2 To (GPT)* pour les raisons indiquées. Gardez cette option par défaut, **Ubuntu** étant un système d'exploitation moderne, il saura gérer sans aucun problème.

En validant sur **Formater**, le logiciel prévient que les changements seront irréversibles :

![Formater un disque - Sélectionner le disque](Format_disk4.png "Formater un disque - Sélectionner le disque")

Valider en cliquant sur **Formater**.

Sauf si vous avez sélectionné l'effacement complet des données, le formatage est rapide, quelques secondes tout au plus, même pour un disque de grande capacité.

Vous disposez donc d'un disque vide avec un espace non alloué :

![Formater un disque - Sélectionner le disque](Format_disk5.png "Formater un disque - Sélectionner le disque")

Il est maintenant possible d'allouer l'espace, c'est-à-dire de définir les différentes parties, nommées *partition*, pour l'usage désiré. Si vous n'avez rien prévu de spécifique, utilisez tout l'espace (donc une seule partition).

Pour créer une partition, il suffit de créer sur le bouton « + » situé sous la section des *Volumes*, à gauche de la roue crantée. Une nouvelle fenêtre s'ouvre :

![Formater un disque - Sélectionner le disque](Format_disk6.png "Formater un disque - Sélectionner le disque")

Choisissez la taille de la partition à créer. Si vous n'avez rien prévu de spécifique, utilisez tout l'espace (ce qui signifie que l'espace disponible suivant doit se trouver à 0) :

![Formater un disque - Sélectionner le disque](Format_disk7.png "Formater un disque - Sélectionner le disque")

Cliquez sur *Suivant*.

Une nouvelle fenêtre s'ouvre pour définir le nom de la partition, s'il faut écraser les données (une nouvelle fois), ainsi que le type du système de fichier.

![Formater un disque - Sélectionner le disque](Format_disk8.png "Formater un disque - Sélectionner le disque")

Donnez lui donc un nom, par exemple ici « Fichiers », n'activez pas l'écrasement des données qui est très long (sauf si vous en avez besoin dans le cas de données sensibles présentes auparavant), et choisissez un format adéquat :

- **Ext4** : pour *Linux* uniquement si vous n'avez pas pour usage de le partager avec un autre système d'exploitation.
- **NTFS** : Si vous comptez utiliser ou partage ce disque avec un système d'exploitation **Windows**.
- **FAT** : pour rendre la partition accessible par tous les systèmes d'exploitation.
- **Autre** : pour un usage spécifique.

Dans l'exemple, nous choisirons *Ext4*.

Cliquez sur *Créer*. Après quelques instants, la partition est créée :

![Formater un disque - Sélectionner le disque](Format_disk9.png "Formater un disque - Sélectionner le disque")

Pour rendre le disque disponible immédiatement sur le système, il faut cliquer sur le bouton « &#x25B6; » situé à gauche de la roue crantée et le disque apparaîtra alors dans le *gestionnaire de fichiers* (application *Fichiers*).

**Remarque** : Cette dernière opération sera automatique pour les fois suivantes à chaque fois que vous connecterez votre disque.

Vous pouvez maintenant ouvrir votre gestionnaire de fichiers (*Fichiers*), accéder au disque et créer/ajouter des fichiers.

## Formater une clef USB

Même s'ils sont de moins en moins utilisés à cause de l'usage de plus en plus intensif des services de stockage en ligne (Cloud), les clefs USB possèdent de nombreuses qualités qui ne rendent pas leur usage caduque pour autant.