# Gestion des répertoires et fichiers

La gestion des fichiers présents sur l'ordinateur est une des fonctions importantes d'un système d'exploitation. Dans la très grande majorité des cas, cette gestion passe par une application nommée «**gestionnaire de fichiers**» et propose une interface graphique ergonomique, mettant à disposition l'ensemble des opérations nécessaires à l'usage normal des fichers et des répertoires.

Sous *Ubuntu*, cette application est simplement nommée «**Fichiers**».

## Gestionnaire de fichiers


### Navigation

## Créer un répertoire

**Note**&nbsp;: un «*répertoire*» est aussi nommé «*dossier*».

Pour créer un répertoire, naviguer jusqu'à l'endroit où vous désirez créer ce répertoire. Une fois là, il y a deux méthodes possibles.

### Création par le bouton de création

Rendez-vous sur la bannière centrale (en haut au centre) indiquant votre position actuelle&nbsp;:

![Fichiers - Bannière](Files-banner1.png "Fichiers - Bannière")

À droite de cette bannière se trouve un bouton avec 3 points verticaux. Cliquez sur ce bouton. Un menu s'ouvre  et vous propose différentes options:&nbsp;:

![Fichiers - menu d'options](Files-create_file1.png "Fichiers - menu d'options")

Choisissez «**Nouveau dossier…**». Une nouvelle fenêtre s'ouvre et vous invite à saisir le nom du répertoire à créer&nbsp;:

![Fichiers - fenêtre de création de dossier](Files-create_file2.png "Fichiers - fenêtre de création de dossier")

Entrez le nom dans la zone prévue à cette effet&nbsp;:

![Fichiers - saisie du nom de répertoire](Files-create_file3.png "Fichiers - saisie du nom de répertoire")

**Remarque**&nbsp;: Le bouton «**Créer**» s'active lors de la saisie.

Cliquez sur «**Créer**» (bouton en haut à droite).

Si un répertoire portant le même nom existe déjà, le bouton «**Créer**» se désactive et un message apparaît en dessous de la zone de saisie&nbsp;:

![Fichiers - le nom de répertoire existe déjà](Files-create_file4.png "Fichiers - le nom de répertoire existe déjà")

Dans ce cas, veuillez choisir un autre nom pour votre répertoire.

### Création par le menu contextuel

Il existe une seconde méthode pour créer un répertoire.

## Renommer un répertoire




## Supprimer un répertoire

Si vous désirez supprimer un répertoire (et son contenu), il suffit d'un clic-droit sur ce répertoire. Un menu contextuel s'affiche alors&nbsp;:

![Fichiers - effacer un répertoire](Files-delete_directory1.png "Fichiers - effacer un répertoire")

Choississez l'option «**Mettre à la corbeille**».

**Astuce**&nbsp;: il est possible de supprimer un élément (répertoire ou fichier) depuis le clavier. Pour cela cliquez (*avec la souris ou le pavé tactile*) sur l'élément à supprimer puis appuyez sur la touche «Suppr.» ou «Delete» de *votre clavier*.

**Remarque**&nbsp;: une notification s'affiche en bas de la fenêtre durant quelques secondes pour vous permettre d'annuler l'opération.

![Fichiers - notification de suppression de répertoire](Files-delete_directory2.png "Fichiers - notification de suppression de répertoire")

Si vous avez supprimé un dossier (ou un fichier) par erreur et que vous n'avez pas eu le temps d'annuler l'opération, pas de panique&nbsp;: il est toujours possible de récupérer l'élément supprimé depuis la *Corbeille*. Consultez la section «**Restaurer un répertoire ou fichier supprimé**».

## Restaurer un répertoire ou fichier supprimé

Si vous avez supprimé un dossier ou un ficher par erreur et que vous n'avez pas eu le temps d'annuler l'opération, vous pouvez toujours le restaurer depuis la *Corbeille*.

Pour cela, se rendre dans la *Corbeille* depuis le gestionnaire de fichiers à partir des raccourcis de navigation (section à gauche)&nbsp;:

![Fichiers - raccourcis de navigation](Files-main_menu1.png "Fichiers - raccourcis de navigation")

Dans la zone principale, au centre, effectuez un clic-droit sur l'élément (répertoire ou fichier) à restaurer&nbsp;:

![Fichiers - restauration de répertoire](Files-restore_directory1.png "Fichiers - restauration de répertoire")

et choisissez l'option «**Restaurer depuis la corbeille**».

**Remarque**&nbsp;: notez que la corbeille vous indique «*Les éléments placés dans la corbeille depuis plus de 30 jours sont automatiquement supprimés*».

![Fichiers - indication de suppression automatique](Files-suppression_banner.png "Fichiers - indication de suppression automatique")

Cette suppression automatique évite que les élements supprimés ne s'accumulent au cours du temps et ne remplissent l'espace disque au point d'empêcher le fonctionnement normal du système d'exploitation. Il est possible de changer le comportement en cliquant sur le bouton «**Paramètres**» (à droite) ou de se rendre dans **Paramètres** > **Confidentialité** > **Historique des fichiers et corbeille**  > **Périodicité de suppression automatique**.


# Rechercher un fichier ou un répertoire

