# Numériser des documents

## Matériel

Pour numériser un document, il faut posseder un scanneur. Aujourd'hui, cet appareil est intégré dans l'imprimante sous la terminologie imprimate «**tout-en-un**» ou «**multi-fonction**».

*Linux* gère la plupart des appareils disponibles sur le marché. Aucune difficulté majeure, il suffit de suivre la notice d'installation du matériel. En cas de problème, chercher sur Internet en précisant&nbsp;:
- votre distribution *Linux* avec sa version (*Ubuntu 22.04*, etc.);
- le modèle du matériel incriminé.

## Lancer le logiciel de numérisation

Une fois que l'imprimante/scanner est allumé(e) et connecté(e) au réseau (par câble ou Wi-Fi) ou à l'ordinateur (par câble USB), et que le document à numériser est placé sur la fenêtre de scanneur, il faut lancer le logiciel «**Numériseur de documents**».
Pour cela, cliquer sur le bouton «**Afficher les applications**», c'est-à-dire le bouton sur la barre d'icônes (ou *Dock*)&nbsp;:

![Dock - Afficher les applications](Ubuntu-display_apps_button.png "Dock - Afficher les applications")

**Astuce**&nbsp;: Le même résultat peut être obtenu simplement un appuyant sur le bouton «*Windows*» du clavier, nommée aussi touche «*Super*» sous Linux. Sur la plupart des claviers, ce bouton ressemble à&nbsp;:

![Clavier - Touche Windows](windows_key.png "Clavier - Touche Windows")

Cette vue permet d'accéder à l'ensemble les applications (icômes), aux espaces virtuels et à **une barre de recherche**&nbsp;:

![Applications - Barre de recherche](Ubuntu-applications_searchbar.png "Applications - Barre de recherche")

Depuis cette zone de recherche, il suffit de taper le nom de l'application ou ce qu'on cherche (courriel, tnternet, …) pour trouver la/les application(s) associée(s).
Taper «**num**» (pour **numériser**) ou bien «**scan**» (pour **scanner**) afin d'afficher l'icône de l'application  «**Numériseur de documents**»&nbsp;:

![Barre de recherche - Numériseur de documents](Ubuntu-scanner_icon.png "Barre de recherche - Numériseur de documents")

**Remarque**&nbsp;: il est aussi possible de trouver une application depuis les icônes affichées (vue «**Afficher les applications**») sans passer par la barre de recherche. Les icônes sont normalement classées par ordre alphabétique.

Cliquer sur l'icône. L'application se lance. Immédiatement, l'application *Recherche des périphériques de numérisation*.

![Numériseur de documents - Recherche de périphériques du numérisation](Scanner-search_devices.png "Numériseur de documents - Recherche de périphériques du numérisation")

Après quelques secondes, elle passe à *Prêt à numériser* 

![Numériseur de documents - Prêt à numériser](Scanner_ready.png "Numériseur de documents - Prêt à numériser")

Elle propose la liste de ce qu'elle a trouvé dans un menu déroulant dont la liste complète est accessible en cliquant sur le chevron (à droite)&nbsp;:

![Numériseur de documents - liste des périphériques disponibles](Scanner-devices_list.png "Numériseur de documents - liste des périphériques disponibles")

Choisir l'appareil qui convient. Si plusieurs peuvent convenir, en choisir un puis lancer une numérisation en cliquant sur «**Numériser**» (bouton en haut à gauche de la fenêtre de l'application).

Si le matériel est indisponible, un message d'erreur apparaît. Dans ce cas, ouvrir la liste des appareils disponibles et en choisir un autre puis «**Numériser**» à nouveau. Si le problème persiste, c'est peut-être parce que le scanneur n'est pas allumé ou connecté correctement au réseau. Vérifiez que tout soit en ordre et rafraîchissez alors la liste en cliquant sur le bouton **&#x21BB;** (*Rafraîchir*) à côté de la liste des périphériques disponibles.

Une fois que le document est numérisé, il apparaît dans la fenêtre de l'application&nbsp;:

![Numériseur de documents - document brut](Scanner-raw_document.png "Numériseur de documents - document brut")

## Manipuler le document

Les documents numérisés peuvent être&nbsp;:
- dans le mauvais sens&nbsp;: c'est le cas lorsqu'on se trompe de positionnement ou que le format du document ne permet pas de le placer correctement.
- mal cadrés&nbsp;: c'est le cas lorsque les documents n'ont pas la même dimension que la fenêtre de l'appareil.

Dans les deux cas, l'interface permet de corriger ces problèmes sans numériser à nouveau.

Dans la zone inférieure de la fenêtre, vous disposez de plusieurs boutons pour effectuer des opérations.

![Numériseur de documents - actions](Scanner-actions.png "Numériseur de documents - actions")

De gauche à droite&nbsp;:
- **pivoter la page vers la gauche** (sens anti-horaire)&nbsp;: permet de pivoter le document sélectionné de 90° dans le sens anti-horaire.
- **pivoter la page vers la droite** (sens horaire)&nbsp;: permet de pivoter le document sélectionné de 90° dans le sens horaire.
- **recadrer**&nbsp;: permet de recadrer le documents pour éliminer les bords excédentaires ou disgrâcieux.
- **supprimer**&nbsp;: supprimer la pager sélectionnée.

### Changer le sens d'un document

Sélectionner la page du document en cliquant dessus, n'importe où sur la zone dans le cadre. Un liseret bleu (ou rouge en fonction de la version du logiciel) apparaît alors sur la page sélectionnée.

Cliquer sur le bouton correspondant à l'action à effectuer, ici pivoter dans le sens horaire ou anti-horaire, jusqu'à obtenir le résultat voulu.

Si le document était à l'envers, une première rotation le fait pivoter de 90°&nbsp;:
![Numériseur de documents - rotation simple](Scanner-rotate_once.png "Numériseur de documents - actirotation simpleons")

Et une seconde rotation le remet dans le bon sens&nbsp;: 
![Numériseur de documents - rotation double](Scanner-rotate_twice.png "Numériseur de documents - rotation double")

## Recadrer un document

Sélectionner la page du document en cliquant dessus, n'importe où sur la zone dans le cadre. Un liseret bleu (ou rouge en fonction de la version du logiciel) apparaît alors sur la page sélectionnée.

Cliquer sur le bouton correspondant à l'action à effectuer, ici recadrer. Une nouvelle zone apparaît sur la page sous la forme d'un cadre au liseret noir ainsi que des traits d'alignement (dépend de la version du logiciel)&nbsp;:

![Numériseur de documents - recadrer](Scanner-crop.png "Numériseur de documents - recadrer")

Le cadre apparaît au milieu par défaut, ou bien au même endroit que le recadrage précédent.

À partir de là, il y a 3 actions possibles&nbsp;:
- déplacer la zone de recadrage;
- ajuster la hauteur;
- ajuster la largeur.

L'objectif est de passer de&nbsp;:

![Numériseur de documents - recadrer](Scanner-crop.png "Numériseur de documents - recadrer")

à&nbsp;:

![Numériseur de documents - document recadré](Scanner-document_cropped.png "Numériseur de documents - document recadré")

### Déplacer la zone de recadrage

En positionnant le pointeur de la souris sur la zone, loin des bords des lignes de démarcation, le pointeur se transforme en main indiquant qu'il est possible de glisser/déplacer la zone entière.

![Numériseur de documents - pointeur de déplacement par glisser/déposer](Scanner-hand_pointer2.png "Numériseur de documents - pointeur de déplacement par glisser/déposer")

Une fois le pointeur sous cette forme, cliquer (clic-gauche) pour verrouiller la souris puis, sans relâcher, déplacer le pointeur jusqu'à l'endroit désiré. Relâcher le pointeur.

### Ajuster la hauteur

En positionnant le pointeur sur une **ligne de démarcation horizontale** (celle du haut ou celle du bas), le pointeur se transforme en double flèche, indiquant qu'il est possible de déplacer la ligne dans un sens ou l'autre.

![Numériseur de documents - recadrage hauteur](Scanner-crop_height.png "Numériseur de documents - recadrage hauteur")

Une fois le pointeur sous cette forme, cliquer (clic-gauche) pour verrouiller la souris puis, sans relâcher, déplacer le pointeur jusqu'à l'endroit désiré. Relâcher le pointeur.

### Ajuster la largeur

En positionnant le pointeur sur une **ligne de démarcation verticale** (celle de droite ou celle de gauche), le pointeur se transforme en double flèche, indiquant qu'il est possible de déplacer la ligne dans un sens ou l'autre.

![Numériseur de documents - recadrage largeur](Scanner-crop_width.png "Numériseur de documents - recadrage largeur")

Une fois le pointeur sous cette forme, cliquer (clic-gauche) pour verrouiller la souris puis, sans relâcher, déplacer le pointeur jusqu'à l'endroit désiré. Relâcher le pointeur.

### Ajuster largeur et hauteur simultanément

En positionnant le pointeur sur **un des angles du cadre de démarcation**, le pointeur se transforme en double flèche orientée en diagonale, indiquant qu'il est possible de déplacer la ligne dans le sens de la hauteur et/ou de la largeur.

![Numériseur de documents - recadrage largeur et hauteur](Scanner-width_height.png "Numériseur de documents - recadrage largeur et hauteur")

Une fois le pointeur sous cette forme, cliquer (clic-gauche) pour verrouiller la souris puis, sans relâcher, déplacer le pointeur jusqu'à l'endroit désiré. Relâcher le pointeur.

## Ajouter une page au document

Il est possible de numériser plusieurs pages dans un même document. Pour cela, après chaque document numérisé, changer la page dans le scanneur et numériser à nouveau (bouton «**Numériser**» en haut à gauche). La nouvelle page vient s'ajouter.

![Numériseur de documents - document 2 pages](Scanner-2pages.png "Numériseur de documents - document 2 pages")

**Remarque**&nbsp;: Il est toujours possible de changer l'orientation ou de recadrer lorsqu'il y a plusieurs pages dans un même document. Il suffit alors de cliquer sur la page correspondante (un liseret apparaît autour de la page sélectionnée) et d'effectuer ensuite les opérations désirées.

## Supprimer une page du document

Sélectionner la page du document en cliquant dessus, n'importe où sur la zone dans le cadre. Un liseret bleu (ou rouge en fonction de la version du logiciel) apparaît alors sur la page sélectionnée.

Cliquer sur le bouton correspondant à l'action à effectuer, ici supprimer (la corbeille) pour obtenir le résultat voulu.

### Que faire si une page est supprimée par erreur ?

Numériser à nouveau la page et la déplacer (voir **Déplacer une page dans document**) jusqu'à sa position initiale ou désirée.

## Déplacer une page dans document

Il se peut que vous ayez numérisé les pages dans le désordre. Si vous voulez replacer une ou plusieurs pages au bon endroit, il suffit d'un clic-droit sur la page à déplacer; un menu contextuel apparaît :

![Numériseur de documents - menu contextuel](Scanner-more_options.png "Numériseur de documents - menu contextuel")

Choisissez «**Déplacer vers la droite**» ou «**Déplacer vers la gauche**» en fonction de l'objectif désiré.

## Enregistrer le document

Lorsque votre document correspond à ce que vous désirez, il faut l'enregistrer.

Pour cela, cliquer sur le bouton «Enregistrer», en haut à droite, le premier en partant de la gauche (sous la formet d'une flèche dirigée vers le bas)&nbsp;:

![Numériseur de documents - enregistrer le document](Scanner-save_document.png "Numériseur de documents - enregistrer le document")

Une fenêtre s'ouvre et vous propose d'enregistrer le document sous un nom par défaut, normalement **Document numérisé**, suivi d'une extension **.pdf**, **.png**, etc.

Après avoir vérifié le format d'enregistnement (voir **Les différents formats d'enregistrement**), enregistrer votre document&nbsp;:

1. naviguer dans jusque dans le répertoire où vous voulez enregistrer le fichier;
2. nommer le fichier conformément à son contenu (pour mieux le retrouver/rechercher) dans la zone de nommage (située en haut, au milieu de la fenêtre);
3. cliquer sur le bouton «Enregistrer» (en haut de la fenêtre, à droite).

Assurez-vous maintenant que le fichier soit bien présent et bien lisible. Pour cela, ouvrez votre gestionnaire de fichiers, rendez-vous dans le répertoire où vous avez enregistré votre document et ouvrez-le en double cliquant dessus. Le «**Visionneur de documents**» s'ouvre et affiche le document final, avec toutes ses pages, dans l'ordre.

![Visionneur de documents - document final](Scanner-final_document_pdf.png "Visionneur de documents - document final")

Une fois que votre document est ouvert et correct, vous pouvez fermer votre application de numérisation ou numériser un autre document en cliquant sur le bouton «**+&nbsp;Nouveau document**» en bas à gauche.

### Les différents formats d'enregistrement

L'enregistrement d'un document peut s'effectueur dans plusieurs formats.

**.pdf** (PDF pour *Portable Document Format* ou *format de document portable*) est le format à privilégier, notamment dans le cas de documents destinés à être lus (donc avec du texte).

Les autres formats (*.png*, *.jpeg*, *.webp*) sont des formats d'images et ne sont utiles que si vous désirez des fichiers au format image.

**Remarque**&nbsp;: de façon générale, il est préférable de choisir le format PDF par défaut. Ce dernier permet une meilleure compatibilité entre les différenets systèmes d'exploitation (*Microsoft Windows*, *Apple MacOSX*, *Apple iPadOS*, *Google Android*, *Linux*, etc.), c'est pourquoi il est «&nbsp;portable&nbsp;». Ce format permet aussi de convertir tout ou partie en images si besoin, mais aussi d'ajouter des pages, d'en supprimer, etc.

Si le nom de fichier par défaut est autre que «Document numérisé.**pdf**», c'est que vous n'enregistrerez pas votre document en PDF.

Pour changer le format d'enregistement, il suffit de se rendre dans la zone en bas à droite de la fenêtre d'enregistrement et vérifier si vous avez le bon. Par exemple, ici un *PDF*&nbsp;:

![Numériseur de documents - sauver au format PDF](Scanner-save_pdf.png "Numériseur de documents - enregistrement au format PDF")

Sinon, cliquer sur le menu et choisissez le format qui vous convient&nbsp;:

![Numériseur de documents - choisir un format d'enregistrement](Scanner-save_choose_format.png "Numériseur de documents - choisir un format d'enregistrement")

Vérifier ensuite que le nom de fichier finit bien avec la bonne extension. Par exemple «ma facture xx.**pdf**». Sinon, modifier à la main.

