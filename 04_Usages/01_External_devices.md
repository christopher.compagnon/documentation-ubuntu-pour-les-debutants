# Connecter du matériel à son ordinateur


## Connecter son téléphone ou sa tablette Android par USB

### Brancher le câble

Déverrouillez votre appareil.

Branchez le câble USB qui correspond à votre appareil (USB/USB-C) Android d'un côté et à votre ordinateur (USB/USB-C) de l'autre.

### Sur l'appareil Android

Une notification devrait apparaître sur l'écran de l'appareil *Android* vous demandant quel type de connexion vous désirez effectuer.


![Invitation Android pour USB](Android_USB_connection.png "Invitation Android pour USB")

Quelle que soit la formulation de la notification qui peut varier en fonction des versions, choisissez celle qui correspond au transfert de fichiers.

### Sur l'ordinateur

Maintenant que vous avez accepté la connexion pour le partage USB, l'ordinateur devrait reconnaître l'appareil comme un disque externe.

Une nouvelle icône devrait apparaître dans la barre des tâches.

![Ubuntu - nouvel appareil connecté](Ubuntu_phone_connection_2.png "Ubuntu - nouvel appareil connecté")

Vous pouvez cliquer dessus pour l'ouvrir ou bien passer par le gestionnaire de fichiers :

![Ubuntu - nouvel appareil connecté](Ubuntu_phone_connection_1.png "Ubuntu - nouvel appareil connecté")

Votre appareil (ici «Nokia 6.1») apparaît dans la section centrale à gauche.
En cliquant dessus, le gestionnaire de fichiers donne accès à son contenu.

Dans l'exemple, il y a deux disques, le stockage interne qui correspond à celui de l'appareil par défaut et, éventuellement, un supplémentaire, (ici «Carte SD Sandisk») si vous avez utilisé une carte d'extension (carte SD).


## Connecter son téléphone Android par Wi-Fi

Il est possible de connecter son téléphone portable *Android* par l'intermédiaire du réseau Wi-Fi. Pour cela, il faut utiliser l'application **GS Connect**.

**Note** : **GS Connect** est une implémentation pour [Gnome](https://fr.wikipedia.org/wiki/GNOME) (l'interface **Ubuntu**) de l'application **KDE Connect** initialement conçue pour l'interface [KDE](https://fr.wikipedia.org/wiki/KDE).

### Installation de KDE Connect sur le téléphone portable

Rendez-vous dans la logithèque de votre téléphone portable (*Google Store*, *Play Store*, *AppGallery*, le nom peut varier en fonction du fabriquant et de la version *Android*), et recherchez l'application **KDE Connect**.

![Android - installer KDE Connect](Android_Install_kdeconnect.png "Android - installer KDE Connect")

Lancez l'application nouvellement installée.

![Android - lancer KDE Connect](Android_launch_kdeconnect.png "Android - lancer KDE Connect")

L'application indique qu'il n'y a pas de périphériques disponibles. Ce qui est normal. Il faut maintenant installer **GS Connect**.

### Installation de GS connect sur l'ordinateur

Depuis **Ubuntu**, il faut installer **GS Connect** pour se connecter à **KDE Connect** de votre téléphone portable.



