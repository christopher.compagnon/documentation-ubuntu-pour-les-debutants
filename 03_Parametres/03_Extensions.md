# Les extensions


Le comportement du système d'exploitation peut-être paramétré ou modifié grâce à des extensions. Il s'agit de petit programmes qui s'installent pour étendre les fonctionnalités du système d'exploitation. Certaines sont de l'ordre du cosmétique, d'autres de la productivité.

## Logiciels

Les extensions peuvent s'installer manuellement, mais il existent des logiciels qui rendent leur gestion plus aisée.

### Gnome Tweaks

Ce logiciel permet d'accéder à des fonctionnalités avancées, comme :

- La mise en veille lorsque l'écran du portable est rabattu :

![Gnome-Tweaks - Général](Gnome-Tweaks1.png "Gnome-Tweaks - Général")

- L'installation de thèmes :

![Gnome-Tweaks - Apparence](Gnome-Tweaks2.png "Gnome-Tweaks - Apparence")

- Les applications au démarrage :

![Gnome-Tweaks - Applications au démérrage](Gnome-Tweaks3.png "Gnome-Tweaks - Applications au démérrage")

- Le comportement des barres de titre des fenêtres :

![Gnome-Tweaks - Barre de titre des fenêtres](Gnome-Tweaks4.png "Gnome-Tweaks - Barre de titre des fenêtres")

- Le comportement de la barre supérieure, notamment le format d'affichage de la date du jour :

![Gnome-Tweaks - Barre supérieure](Gnome-Tweaks5.png "Gnome-Tweaks - Barre supérieure")

- Le comportement du clavier, de la souris et du pavé tactile :

![Gnome-Tweaks - Clavier et souris](Gnome-Tweaks6.png "Gnome-Tweaks - Clavier et souris")

- Le comportement des fenêtres :

![Gnome-Tweaks - Fenêtres](Gnome-Tweaks7.png "Gnome-Tweaks - Fenêtres")

- Le choix de polices, leur style et leur taille :

![Gnome-Tweaks - Polices](Gnome-Tweaks8.png "Gnome-Tweaks - Polices")


Ce logiciel est accessible dans la logithèque (en recherchant «tweaks») :

![Gnome-Tweaks - installation](Gnome-Tweaks.png "Gnome-Tweaks - installation")

#### Installation alternative (avancée) :

Si le logiciel n'apparaît pas dans le dépôt logiciel, il est possible de l'installer manuellement par la ligne de commande :

    sudo apt install gnome-tweaks


### Extension Manager

Logiciel tiers permettant non seulement de gérer et configurer les extensions, mais aussi de les rechercher et les installer (contrairement à **Extensions**, qui ne fait que les gérer).

Ce logiciel est accessible dans la logithèque (en recherchant «extension») :

![Gnome-Extension-Manager - installation](software-extension.png "Gnome-Extension-Manager - installation")

#### Installation alternative (avancée) :

Si le logiciel n'apparaît pas dans le dépôt logiciel, il est possible de l'installer manuellement par la ligne de commande :

    sudo apt install gnome-shell-extension-manager


### Extensions

Logiciel fourni par les développeurs du projet **Gnome** afin de gérer et configurer les extensions installées.

![Gnome-Extensions - installation](gnome-extensions.png "Gnome-Extensions - installation")

Contrairement à **Extension manager** qui permet de rechercher et installer les applications, **Extensions** ne fait que les gérer. Pour les installer, il faut passer par le site web https://extensions.gnome.org.

Ce logiciel est accessible dans la logithèque (en recherchant «extension») :

![Gnome-Extension-Manager - installation](software-extension.png "Gnome-Extension-Manager - installation")

#### Installation alternative (avancée) :

Si le logiciel n'apparaît pas dans le dépôt logiciel, il est possible de l'installer manuellement par la ligne de commande :

    sudo apt install gnome-shell-extension-prefs



## Quelques extensions


### GS Connect

GS Connect permet de connecter un téléphone Android à son ordinateur et de le gérer à distance.

Cette extension est disponible depuis le site web https://extensions.gnome.org, depuis la logithèque (en recherchant «GSConnect») ou avec **Extension Manager**.

#### Installation alternative (avancée) :

Si le logiciel n'apparaît pas dans le dépôt logiciel, il est possible de l'installer manuellement par la ligne de commande :

    sudo apt install gnome-shell-extension-gsconnect


### Sound Input & Output Device Chooser

Lorsque plusieurs périphériques de son sont installés, pour changer l'entrée ou la sortie, il faut passer par les **Paramètres > Son**.

![Paramètres - son input output](Parametres_son_input_output.png "Paramètres - son input output")

Ce qui est long et fastidieux.

Cette extension permet de disposer de raccourcis dans le menu des options afin de choisir l'entrée et/ou la sortie plus aisément, sans passer par les **Paramètres**.

![Extension - son input output](Son_input_output_extension.png "Extension - son input output")

**Note** : cette extension est rendue caduque par la version **Ubuntu 22.10** qui intègre navivement cette option.

### Tactile

Par défaut, **Gnome** propose trois positions d'affichage des applications :
- le plein écran;
- la moitié à droite;
- la moitié à gauche.

Et c'est tout ! Si l'on veut autre chose, il faut le gérer à la main, à l'aide de la souris (ou tout autre dispositif de pointage).

Il peut être utile d'utiliser automatiquemt des portions plus petites ou plus grandes, notamment sur les grands écrans.

C'est là que **Tactile** intervient.

![Extension - Tactile](Extension_Tactile.gif "Extension - Tactile")

