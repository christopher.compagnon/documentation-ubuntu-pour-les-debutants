# dconf

**dconf** est une sorte de base de données qui permet au système d'exploitation et aux applications de stocker des informations sur leur configuration et leur comportement.

Ce format n'est pas aisé à lire nativement. Il existe cependant un logiciel, **dconf editor**, conçu pour faciliter cette tâche.

Si **Gnome** ne permet pas de modifier finement les configurations, c'est plus par philosophie que par technologie. L'objectif est de disposer d'une interface épurée mais efficace.

Si l'on veut modifier les comportements ou étendre les possibilités, il faut soit accéder à **dconf**, soit installer des extensions.


## dconf editor

**dconf editor** (ou **Éditeur Dconf**) permet d'accéder à *dconf* et de modifier la configuration depuis une interface compréhensible et aisée à parcourir.

Ce logiciel est accessible dans la logithèque (en recherchant «dconf») :

![dconf editor - installation](dconf-editor.png "dconf editor - installation")

Une fois lancée, l'application ressemble à ceci :

![dconf editor - application](dconf.png "dconf editor - application")

Il suffit de naviguer dans les arborescences jusqu'à la clef désirée, de définir la nouvelle valeur et de valider.

## Quelques configurations

### Enregistrer la session

**Gnome** peut enregistrer les applications ouvertes au moment de la fermeture de la session et peut alors les restaurer à la reconnexion.

Pour cela, se rendre dans **org > gnome > gnome-session** et activer la clef **auto-save-session**.

### Changer le format de compression par défaut

Par défaut, le format de compression est *.zip*, un format qui permet d'être échangé avec les autres systèmes d'exploitation – comme **Windows**. Il est possible de changer ce format.

Pour cela, se rendre dans **org > nautilus > compression > default-compression-format**, désactiver la valeur par défaut et sélectionner une valeur personnalisée dans la liste : *tar.xz*, *7z*, *chiffré*.

### Changer le pas de réglage du son

Par défaut, l'ajustement du son depuis le clavier s'effectue par intervalles de 6%. Il est possible de changer cette valeur depuis **org > gnome > settings-daemon > plugins > media-keys** en modifiant la valeur de la clef **volume-step**.