# Installation et configuration d'imprimante

## Installation d'imprimante




## Une fois l'imprimante installée

Une fois que l'imprimante est installée, tout n'est pas fini. Il faut se préparer à d'éventuels problèmes inhérents à l'usage d'un tel périphérique.

### Télécharger la documentation de l'imprimante

L'imprimante est fournie avec une documentation au format papier. Document qu'on a tôt fait de poser dans un coin et de ne pas retrouver le moment venu.
Pour ne pas le chercher, le mieux est de télécharger sa version PDF et de la placer sur l'ordinateur, dans un répertoire bien spécifique.

Cette documentation est généralement disponible sur le site du constructeur.

### Noter la procédure d'accès au serveur d'impression embarqué

Si vous imprimante fonctionne en réseau (câble ou Wi-Fi) alors il y a de fortes chances qu'elle dispose d'un micrologiciel lui permettant d'accéder à ses fonctionnalités depuis un navigateur web.
Ainsi, cette interface permet généralement de :
- mettre à jour le micro-logiciel;
- vérifier l'état des réserves d'encre;
- nettoyer/aligner les têtes d'impression;
- d'autres fonctionnalités plus avancées, en fonction du matériel, de sa version et du constructeur.

L'accès à cette interface web est généralement expliquée dans la documentation de l'appareil.

Il est fortement recommandé de savoir comment y accéder, de noter la procédure dans un document placé au même endroit que la documentation. Il sera probablement nécessaire d'avoir un utilisateur/mot de passe pour y accéder : assurez-vous de les trouver et de les conserver à votre disposition.

L'interface web est accessible depuis le navigateur en entrant son adresse IP. Il y a deux façons de connaître cette adresse :
- en l'imprimant depuis l'imprimante : les imprimantes modernes disposent généralement d'une fonction imprimant les informations la concernant comme la version du micrologiciel, le nombre de pages imprimées et, si elle est connectée au réseau, son adresse IP. L'impression se faisant généralement en langue anglaise, cherchez une information notée *IP Address*.
- en la récupérant depuis le routeur (ou la box Internet) : si l'imprimante est connectée au réseau, alors le routeur lui a attribué une adresse. En se connectant à ce dernier, vous trouverez l'adresse IP de tous les oblets connectés, dont votre imprimante.

**Attention** : la plupart du temps, l'adresse IP est dynamique, ce qui veut dire qu'elle peut changer au cours du temps. L'adresse que vous aviez notée hier n'est peut-être plus la même que celle d'aujourd'hui. Prenez donc soin de noter la procédure pour trouver l'adresse IP de votre imprimante, ce qui vout dire se connecter à votre routne/box Internet le cas échéant. Normalement, son adresse IP à lui ne change pas. Pour le routeur, vous devez disposer d'un utilisateur/mot de passe : assurez-vous de les trouver et de les conserver à votre disposition.

### Imprimer une page de test

#### Depuis l'ordinateur

Cette page de test est à imprimer et à garder précieusement soit sous forme papier soit sous forme numérique, sur votre ordinateur, dans un endroit ou vous ne le perdrez pas.

Pour imprimer une page de test depuis l'ordinateur :

1. Rendez-vous dans **Paramètres > Imprimantes**;
2. Ouvrez les options correspondant à votre imprimante (les trois points à droite) :

![Imprimante - les différentes options](Imprimantes_options.png "Imprimante - les différentes options")

3. Cliquez sur **Options d'impression**;

![Imprimante - Options d'impression](Imprimante_option_impression.png "Imprimante - Options d'impression")

4. Cliquez sur **Page de test** (bouton en haut à gauche).

Si une page de test s'imprime, vérifiez qu'elle ressemble à ceci :

![Imprimante - résultat page de test](Ubuntu_page_de_test.png "Imprimante - résultat page de test")

**Remarque** : Le nom de l'imprimante et les différentes informations peuvent varier en fonction de l'imprimante. Retenez surtout les disques de couleur.

Si vous disposez d'un scanner, numérisez cette page. Elle vous servira alors de point de comparaison. Il est fortement recommandé de la placer avec la documentation de l'imprimante correspondante pour ne pas la perdre et ne pas la mélanger.

#### Depuis l'imprimante

Il est générlament possible d'imprimer une page de test depuis l'imprimante. Cette page diffère de celle d'Ubuntu. Consultez la documentation pour l'imprimer et, de la même façon que précédamment, conservez cette page préceusement.
