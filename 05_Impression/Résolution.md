# Résolution des problèmes d'impression

Nous partirons ici du pré-requis que l'imprimante est connectée au secteur et allumée. Vérifiez donc que ce soit le cas.

## Pas d'impression

Vous désirez imprimer un document et malgré vos tentatives, aucune page ne s'imprime.

### Vérifier les indicateurs

Si l'imprimante possède un écran, vérifiez qu'il n'y ait aucun avertissement affiché.
Vérifiez également que les boutons soient allumés comme il faut.

Veuillez vous référer à la documentation de votre appareil pour connaître précisément l'état normal de l'affichage.

### Vérifier que l'imprimante fonctionne

Tentez alors d'imprimer une page par défaut depuis ce périphérique (c'est-à-dire sans passer par l'ordinateur). Il est normalement possible d'imprimer soit une page de test, soit une page de paramètres. Pour cela, veuillez consulter la documentation de votre imprimante.

Si votre imprimante imprime cette page correctement, c'est qu'elle fonctionne. La page de test doit normalement imprimer du noir (avec des dégradés) et des couleurs (avec des dégradés). Vérifiez que les couleurs apparaissent normalement. Si besoin, comparez cette impression avec l'étalon généré lors de l'installation (voir **[Installation et configuration d'imprimante](./05_Impression/Installation.md) > Imprimer une page de test > Depuis l'imprimante**) afin de déterminer quelles couleurs manquent.

Si des couleurs manquent, c'est qu'il doit y avoir un problème de positionnement de cartouche, de compatiblité (dans le cas de cartouches *compatibles* et non *officielles*), de niveau d'encre ou de tête d'impression bouchée. Voir la section **Impression de pages blanches**.

Si tout est normal, alors c'est un problème de connexion de configuration au niveau de l'ordinateur.

### Vérifier la connexion

Si votre imprimante est connectée au réseau, elle doit indiquer l'état de sa connexion sur son cadran (ou son écran LCD). Reportez-vous à la documentation de votre appareil pour déterminer l'état de sa connexion.

**Attention** : L'imprimante peut être connectée, mais pas forcément de la bonne façon. Elle peut utiliser le Bluetooth ou bien une connexion WiFiDirect. Dans ce cas, même si elle apparaît parfaitement connectée, elle peut ne pas communiquer avec votre ordinateur (mais peut-être votre téléphone). Reportez-vous à la documentation de votre appareil pour déterminer le statut de sa connexion.

Si votre imprimante possède une fonction de numérisation, introduisez un document dans le scanner et numérisez une page. Si la numérisation fonctionne, cela signifie que le système d'exploitation voit l'imprimante. Il n'y a donc aucun problème de connexion. Votre ordinateur a bien trouvé l'appareil et est capable de l'exploiter. Le problème d'impression ne concerne très certainement pas la connexion.

Si votre imprimante ne possède pas de scanner mais qu'elle est connectée au réseau, en imprimant ses paramètres, elle devrait vous donner une adresse IP (voir [Installation et configuration d'imprimante](./05_Impression/Installation.md) pour savoir comment imprimer ces informations). Suivez la procédure et vérifiez si vous trouvez cette adresse.

### Vérifier la file d'attente

Il se peut qu'un document bloque la file d'attente de votre imprimante.

Depuis votre ordinateur, rendez-vous dans **[Paramètres > Imprimantes](../02_Interface/02_Paramètres.md)**. 

Si vous voyez un document en pause, tentez de le relancer. Si cette action est sans effet et/ou que tous les documents dans la file sont en attente, c'est que le problème n'est pas là.

Dans ce cas, videz la file d'attente et imprimez une page de test depuis votre ordinateur.

### Imprimer une page de test

Pour imprimer une page de test depuis l'ordinateur :

1. Rendez-vous dans **[Paramètres > Imprimantes](../02_Interface/02_Paramètres.md)**;
2. Ouvrez les options correspondant à votre imprimante (les trois points à droite) :

![Imprimante - les différentes options](Imprimantes_options.png "Imprimante - les différentes options")

3. Cliquez sur **Options d'impression** :

![Imprimante - Options d'impression](Imprimante_option_impression.png "Imprimante - Options d'impression")

4. Cliquez sur **Page de test** (bouton en haut à gauche).

Si une page de test s'imprime, vérifiez qu'elle ressemble à ceci :

![Imprimante - résultat page de test](Ubuntu_page_de_test.png "Imprimante - résultat page de test")

**Remarque** : Le nom de l'imprimante et les différentes informations peuvent varier en fonction de l'imprimante. Retenez surtout les disques de couleur.

Observez d'abord les cercles colorés et notez si un ou plusieurs sont manquants. Si c'est le cas, alors c'est qu'il y a un problème au niveau de la cartouche ou de l'encre.

Vérifiez si la cartouche correspondante est positionnée correctement. Pour cela, consultez la documentation de votre appareil.

Vérifiez que vous disposez assez d'encre.

Si tout est correct, c'est qu'il y a peut-être une buse bouchée. Il faut donc **nettoyer les têtes d'impression**.

### Nettoyer les têtes d'impression

Pour nettoyer les têtes d'impression, veuillez consultez la documentation de votre appareil.

Si votre imprimante est connectée au réseau, il est probablement possible de lancer cette procédure en passant par l'interface web du micrologiciel (voir [Installation et configuration d'imprimante](./05_Impression/Installation.md) pour savoir comment y accéder).

Une fois les têtes d'impressions nettoyées, si la procédure n'a pas prévu d'impression de page de test, imprimez-en une (consulter la documentation de votre appareil et suivre la procédure).

Si la page de test s'imprime correctement, tentez d'imprimer votre document à nouveau après avoir vidé la file d'attente (voir section **Vérifier la file d'attente** plus haut).

Si le problème persiste, réinstallez votre imprimante (voir section **Réinstaller l'imprimante** plus bas).

### Imprimer depuis un autre logiciel

Le logiciel que vous utilisez peut être mal paramétré et l'imprimante est alors dans l'incapacité d'imprimer le document (parce que l'impression sort des marges, …).

Imprimez le document depuis un autre logiciel. Pour cela, imprimez votre document dans un fichier, au format PDF, ouvrez-le avec le **visionneur de documents** et tentez de l'imprimer depuis là.

Si ça ne fonctionne toujours pas ou que le problème d'impression se produisait initialement depuis le **visionneur de documents**, ouvrez le document PDF depuis votre navigateur Web et tentez de l'imprimer depuis là.

Si l'impression s'effectue sans problème, c'est que le problème provenait du logiciel. 

Si le problème persiste, réinstallez votre imprimante (voir section **Réinstaller l'imprimante** plus bas).

## Impression de pages blanches

Vous désirez imprimer un document et malgré vos tentatives, les pages sortent de l'imprimante parfaitement blanches.

Si l'imprimante n'imprime que des pages blanches, c'est qu'il y a probablement un défaut d'encre. 
Imprimez une page de test pour déterminer quelles couleurs sont manquantes. S'il ne s'agit que d'une couleur correspondant à une cartouche spécifique, c'est probablement un problème d'encre ou de cartouche.

### Cas d'un problème sur une couleur spécifique

Veuillez vérifier que la cartouche soit installée correctement et qu'aucun indicateur lui correspondant n'indique une alerte (pour le savoir, veuillez consulter la documentation de votre imprimante).

La marque de la cartouche est-elle celle du fabriquant de l'imprimante ? Si non, peut-être devriez-vous tester avec une cartouche du fabriquant et non une cartouche «compatible». Dans certains cas, l'encre utilisée par les versions «compatibles»  peut être différente et sécher plus rapidement.

Veuillez vérifier les niveaux d'encre de votre imprimante. Pour cela, consultez la documentation de votre appareil.

Si tout semble normal, tentez d'imprimer après avoir remplacé la cartouche par une autre neuve.

Si l'impression fonctionne après remplacement (que la cartouche soit officielle ou *compatible*), il s'agissait probablement d'un petit problème de connexion entre la cartouche et l'imprimante, ou bien d'encre sèche.
Si le problème survient toujours sur une cartouche *compatible*, peut-être devriez-vous envisager l'usage de cartouches officielles, plus chères mais plus fiables (dans votre cas).

Si le problème se règle par le remplacement d'une cartouche *compatible* par une autre, alors c'était probablement un problème d'encre sèche. Le rythme d'impression trop faible a induit un séchage de l'encre au niveau de la buse ce qui peut poser quelques problèmes. 
**Solution** : tentez d'imprimer régulièrement une page afin de déterminer si le problème se règle de cette façon.

Si les problèmes persistent, ça peut-être aussi un problème de micro-logiciel qui gère difficilement la cartouche compatible.

**Solution** : tentez d'investir dans une cartouche officielle.

### Cas d'un problème sur toutes les couleurs

Les cartouches sont-elles officielles ou **compatibles** ? Si compatibles, veuillez da'bord consulter le **Cas d'un problème sur une couleur spécifique**.

Nettoyez les têtes d'impression.

Vérifiez que les niveaux d'encre soient corrects. Si tout semble normal, tentez d'imprimer après avoir remplacé une cartouche par une autre neuve.

Si le problème persiste, réinstallez votre imprimante (voir section **Réinstaller l'imprimante** plus bas).

Si le problème persiste, utilisez le pilote correspondant à votre imprimante (voir section **Définir le pilote l'impression** plus bas).

## Réinstaller l'imprimante

Pour réinstaller l'imprimante, procéder comme suit :

1. Rendez-vous dans **[Paramètres > Imprimantes](../02_Interface/02_Paramètres.md)**;
2. Ouvrez les options correspondant à votre imprimante (les trois points à droite) :

![Imprimante - les différentes options](Imprimantes_options.png "Imprimante - les différentes options")

3. Cliquez sur **Supprimer l'imprimante**. Cette dernière doit disparaître de la liste.
4. Cliquez sur **Ajouter une imprimante** (bouton en haut de la fenêtre à droite) :

![Imprimantes - Ajouter une imprimante](Imprimantes_ajouter.png "Imprimantes - Ajouter une imprimante")

Ubuntu recherche alors les imprimantes disponibles. Si votre imprimante est allumée et branchée/connectée correctement alors il doit la proposer.

![Imprimantes - Ajouter une imprimante](Imprimantes_ajouter_propositions.png "Imprimantes - Ajouter une imprimante")

5. Sélectionnez l'imprimante en cliquant dessus. Si vous disposez de plusieurs imprimantes dans la liste alors que vous n'en avez qu'une seule chez vous, pas de panique, choisissez la première de la liste pour commencer :

![Imprimantes - Ajouter une imprimante](Ajout_imprimante_sélection.png "Imprimantes - Ajouter une imprimante")

6. Cliquez sur **Ajouter** (bouton en haut à droite de la fenêtre).

### Définir le pilote l'impression

La pilote d'impression par défaut (nommée «driverless», c'est-à-dire «sans pilote») fonctionne la plupart du temps sur la plupart du matériel. Il peut arriver parfois que cette configuration ne fonctionne pas ou ne soit pas optimale.

Pour forcer un pilote spécifique correspondant au péripérique, procéder comme suit :

1. Rendez-vous dans **[Paramètres > Imprimantes](../02_Interface/02_Paramètres.md)**;
2. Ouvrez les options correspondant à votre imprimante (les trois points à droite) :

![Imprimante - les différentes options](Imprimantes_options.png "Imprimante - les différentes options")

3. Cliquez sur **Informations sur l'imprimante** :

![Imprimante - Informations sur l'imprimante](Imprimante_informations_supplémentaires.png "Imprimante - Informations sur l'imprimante")

Vous disposez de 3 options en bas :
- **Rechercher des pilotes** : le système d'exploitation tente de trouver le pilote adéquat.
- **Sélectionner à partir de la base de données…** : vous sélectionner manuellement votre pilote à partir d'une liste de constructeurs/modèle d'imprimante.
- **Installer le fichier PPD…** . vous disposez du pilote sur la forme d'un fichier PPD (**P**ostScript **P**rinter **D**escription) correspondant à votre matériel.

4. Choisissez l'option **Sélectionner à partir de la base de données…**  et cherchez votre matériel/série/version dans la liste proposée, puis validez.

