# À qui s'adresse cette documentation ?

Cette documentation s'adresse aux débutants et aux simples mortels.

La question sous-jacente reste : « Si je devais l'expliquer à mon père/ma mère, … »

L'objectif est de permettre à une personne non habituée à l'informatique de comprendre le système d'exploitation **Ubuntu** (et plus largement **Linux**) et de le maîtriser progressivement.

Sauf cas particuliers qui serviront plus d'exemple que de passage obligé, il ne sera donc ici nulle question de configuration avancée élitiste reposant sur de la ligne de commande.

